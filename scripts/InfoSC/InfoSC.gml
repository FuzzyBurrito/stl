if global.selected_version = "4.4-pc"
{
	global.info_text = "Various scenario bug fixes\nVarious sprite bug fixes\nGrammar and typo fixes\nNew backgrounds and expressions"
	exit
}
if global.selected_version = "4.3-pc"
{
	global.info_text = "More and more typo fixes\nFixes various crashes and exceptions\nThe scenario button only appears when all scenarios are loaded\nFixes various sound bugs and removes voices/beeps\nSprite viewer finds default sprite instead of a_0\nThe RAM limit on Windows is now 4GB\nSupport for multiple hooks for one scenario\nA small blip has been added to the title screen of scenarios notifying you of their non-canonicity"
	exit
}
if global.selected_version = "4.2-pc"
{
	global.info_text = "Enables developer mode by default\nFixes more typos\nFixed a bug that resulted in custom sound effects and music not being properly loaded for scenarios\nVarious improvements to scenarios including better video/movie support\nGalleries are now sorted alphabetically"
	exit
}
if global.selected_version = "4.1.1-pc"
{
	global.info_text = "Fixes character database from not showing some characters\nStops crashing when trying to save\nSpelling and grammar fixes"
	exit
}
if global.selected_version = "4.0-pc"
{
	global.info_text = "Now with 600.000 words!\nMany changes to make the game start faster and run smoother\nA new format for custom characters\nimprovements to JohnGB/Jane\nMany new sprites,backgrounds and CG's\nAdds Galleries, new sprite viewer, and outsourced scenario packages"
	exit
}
if global.selected_version = "3.1-pc"
{
	global.info_text = "Various bug fixes with scenarios and animations\nScenarios can now be made without having to activate developer mode\nAdds search function to sprite viewer and character database\nImproves John Gym sprite\nAdds some Rachel sprites"
	exit
}
if global.selected_version = "3.0-pc"
{
	global.info_text = "New content release!\nIntroduced a new GUI system\nIntroduced a Character Database\nRevamped Scenario Mode\nRevamped the GUI for instant-messaging conversations\nRevamped the GUI for notes and letters\nRevamped the saving/loading menu with better pagination and navigation\nImproved Intro animation\nAdded two new swap animations\nAdded support for parallel morphs\nVarious bugfixes and spelling fixes\nImage/audio file size optimizations"
	exit
}
if global.selected_version = "2.2-pc"
{
	global.info_text = "Fixed [label duplication] and [ambiguous expression] bugs that affected many users (should now work properly on macOS)\nVarious bugfixes and spelling fixes\nImage/audio file size optimizations"	
	exit
}


global.info_text = "This info is under construction."	
