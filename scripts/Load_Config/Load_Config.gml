ini_open("config.ini");

global.selected_version = ini_read_string("config", "download_mode", "4.4");

global.number = ini_read_string("version", "latest", "");

global.latest_ver = ini_read_string("config", "latest_ver", "4.0");

global.start_close = ini_read_real("config", "start_close", 0);

global.warning = ini_read_real("config", "warning", 0);

global.cache = ini_read_real("config", "cache", 0);

ini_close();