/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4B755747
/// @DnDArgument : "var" "global.selected_version"
/// @DnDArgument : "value" ""Latest""
if(global.selected_version == "Latest")
{
	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 43A53D3B
	/// @DnDParent : 4B755747
	/// @DnDArgument : "code" "ini_open("VersionInfo.ini");$(13_10)global.state = ini_read_string("Launcher", "state", "0");$(13_10)game_url = ini_read_string(global.number, "download", "");$(13_10)global.lib = ini_read_real(global.number, "lib", 0);$(13_10)global.info = ini_read_string(global.number, "info", "");$(13_10)ini_close();"
	ini_open("VersionInfo.ini");
	global.state = ini_read_string("Launcher", "state", "0");
	game_url = ini_read_string(global.number, "download", "");
	global.lib = ini_read_real(global.number, "lib", 0);
	global.info = ini_read_string(global.number, "info", "");
	ini_close();
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 0BA9D956
else
{
	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 2708609B
	/// @DnDParent : 0BA9D956
	/// @DnDArgument : "code" "ini_open("VersionInfo.ini");$(13_10)global.state = ini_read_string("Launcher", "state", "0");$(13_10)game_url = ini_read_string(global.selected_version, "download", "");$(13_10)global.lib = ini_read_real(global.selected_version, "lib", 0);$(13_10)global.info = ini_read_string(global.selected_version, "info", "");$(13_10)ini_close();"
	ini_open("VersionInfo.ini");
	global.state = ini_read_string("Launcher", "state", "0");
	game_url = ini_read_string(global.selected_version, "download", "");
	global.lib = ini_read_real(global.selected_version, "lib", 0);
	global.info = ini_read_string(global.selected_version, "info", "");
	ini_close();
}