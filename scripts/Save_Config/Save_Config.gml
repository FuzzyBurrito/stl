ini_open("config.ini");

ini_write_string("config", "download_mode", global.selected_version);

ini_write_real("config", "start_close", global.start_close);

ini_write_real("config", "warning", global.warning);

ini_write_real("config", "cache", global.cache);

ini_close();