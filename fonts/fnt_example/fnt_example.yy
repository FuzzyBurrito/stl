{
    "id": "c5a57564-1d3b-4f86-b454-f221694ee83d",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_example",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f01d3ba3-2db0-4be6-a512-55b43e1cf8c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 55,
                "y": 65
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f07686f8-3ec7-48c1-b689-b982a165d0cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 16,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 130,
                "y": 65
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2eaa1513-892c-4918-8793-1ed878a8319e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 8,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 99,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ce58fb51-8b2c-4e1c-ac87-b958bd1ae17b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 134,
                "y": 23
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "82f236e3-9d9a-4fb8-8216-8397009068b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 80,
                "y": 23
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7161c632-536b-4aca-b57c-a05d9f13e0e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 16,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3241eadf-cf21-4d08-aafa-14d98eec2137",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a6d067d6-8f31-4f39-8161-ab9a5a0bb61c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 8,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 146,
                "y": 65
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7e6cac31-236f-4f47-b44e-76641f30d3a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 49,
                "y": 65
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a4991222-28bf-4a67-bbac-a5d9b1dce8ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 43,
                "y": 65
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "aa3714d7-9878-44d5-856c-abfdeb884e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 92,
                "y": 65
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d39879cf-067f-47fb-8b12-dcde4daaf2e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 109,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d85e22cf-9508-4b96-9102-6e0758175152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 110,
                "y": 65
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f3c9fc3b-85fd-4da4-a4a0-ca7ddf67b720",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 61,
                "y": 65
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "eefa8e71-c85f-4244-9c72-f5d3ee13ef5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 16,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 134,
                "y": 65
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0ae5eb41-941b-4f4c-8f99-d7fc2cac5dd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1450a8fc-7707-4bb1-a097-be82f7235d6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "397d511d-90f3-42e0-aceb-28a32e136bd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 16,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 23,
                "y": 65
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "32d3a4db-4757-451a-9e51-1f6c421195e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 223,
                "y": 23
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6f61a35a-f2fa-4524-8d0b-0dee2201457a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8498d67b-e5b0-45ff-af13-0df949b6a219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 213,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5dc16809-0e15-4c08-b25c-1e7d5390595c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "dfc78b79-6950-4cc2-9f0c-a7ad6b09f373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7905513a-9479-4e5c-b183-f02cbce51bcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e75677ae-b810-4f81-b4ba-8a87de223d19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 203,
                "y": 23
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b8ace7a0-8258-480a-94e2-cd04be5a65cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 183,
                "y": 23
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1b3ea924-76b9-4bf6-9d1c-c6a96def431d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 16,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 114,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d6df3b39-69c8-4b1c-b6e2-6484a07ace4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 106,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "fa45120e-5e60-49f4-b4be-e8121f8fa8ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 72,
                "y": 44
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "eb1434e0-b22a-42e8-b4e5-7dba3a2b7e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 12,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 220,
                "y": 44
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "79fc4503-57dd-4e6f-abf7-79b14aeaf29c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 128,
                "y": 44
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1867d639-e243-484f-8e46-6f7cdbcdf977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 243,
                "y": 23
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7c565b8f-8bbe-4821-a74a-bbe2583741c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c1fd35e9-b842-4381-b697-e3fd1cdf000b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b462bff0-0bb6-471c-b802-cd9b11c27a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 16,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 112,
                "y": 23
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "869c1e48-318b-4239-8a43-97fdc45564ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f6a9906d-5f7c-48f2-b973-a1c20b6b12e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 16,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c95e7edb-b33a-4cf3-a6e9-16474e476e3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 16,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4a80a863-9522-49e9-ad7e-058f8155c190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 16,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 44
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "bb6839cd-37e4-4962-8fcd-31ff19c4b945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 16,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5b5989f7-9857-4eda-82e3-eb67188a420f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 16,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 58,
                "y": 23
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7528bbf9-8a1a-4e65-b7c4-4b2dd145ad45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 16,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 126,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5229d2ca-c92a-4678-a388-fea9c9fe4fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 91,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b7c1e6a2-adf6-4a7e-b10f-cfacba9fcc03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 16,
                "offset": 2,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 23
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "bc6731c0-9bd2-46f2-a22e-4387238a4d8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 16,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 138,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "cf404d9d-5107-4e2b-941e-6df31b8188a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 16,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0962e585-36d2-4524-b3cb-9840a22be912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 16,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 123,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "eb754a01-0fe0-4297-b55c-f39f3c94f5c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 16,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "03456dff-9b3e-43c9-9a92-03d2834ffe26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 16,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 90,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ed4dee1d-4ca2-491a-a28b-734e353fda47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1e034948-6dfa-455d-b65c-c007ae491589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 16,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 23
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4c820ed5-87be-4d5e-9fd0-b6ae6bf61494",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 16,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 69,
                "y": 23
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1f7a1496-6e6d-437c-856f-713fef25a846",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7fd6d091-9f37-44a4-96a1-4a843bba8ae7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 16,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 101,
                "y": 23
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8858add3-b268-49f1-9566-c789f606266b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "dc137dbe-3a05-454e-8f78-1e28db51f421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "764bfaa8-4f37-41f0-8239-f34b3002edea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7df45eb9-14df-49dd-a49e-46f8c48db31b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c748b778-e01c-4aea-a060-40fad8ea8106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "eb858977-b8a5-4c0f-aa4e-f4662a5f48dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 82,
                "y": 65
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "8492ddfc-e1ba-4d66-9f3f-5da5372717dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 65
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "be9afa84-2f64-4901-b34b-eded97474ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 87,
                "y": 65
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0a56a1d7-3420-4355-b240-3426c5d884dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 10,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 74,
                "y": 65
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e748c70e-4e21-4180-826c-6f6655ba015f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4de288dc-9855-4ab0-bf20-8a895f411a6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 6,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 141,
                "y": 65
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8bbd9cf8-c14d-488a-9521-51f8895a42b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 193,
                "y": 23
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "999603c7-02fb-48c8-8419-ccca6e16fb22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 82,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7062483e-0d21-4f48-81ec-0624d5c8848b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 16,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 165,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "42d48428-999b-48bc-ab3f-d5d2c7a51cb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 174,
                "y": 44
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "edbb1c4c-0a9c-460e-872f-cf8ed3473e7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 173,
                "y": 23
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6e7c8863-81aa-4cbf-bfd4-29245553d171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 16,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 65
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c75c6f69-88dd-4e7b-a708-415beacc302d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 38,
                "y": 23
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7520bbea-b20e-4660-a21a-3e5161e4b0ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 156,
                "y": 44
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9dbb1e67-9e70-458f-aa9d-ddb4da1b9bc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 122,
                "y": 65
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0395cc7c-21c1-4d43-865e-771b27bad055",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 37,
                "y": 65
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ca6f02ca-97be-4db9-ba08-e4f5768a7fa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 16,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 100,
                "y": 44
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b17991ef-ba5d-4809-a549-1154d706efe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 118,
                "y": 65
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3b30a8f3-4345-4229-b65f-f74b4498c0c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 16,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5b51c3f9-0198-44ad-90ef-c76a48ba95e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 119,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a3478bfd-73bd-4d34-8357-19986a626a53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 163,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "62bbd1da-8b95-4483-b343-3cefadb67eb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 145,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1911a3f0-7b24-47c9-af67-b8d1bd526529",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 154,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4ed214ca-bfdf-4941-9202-3a75fcda917c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 16,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 65
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "dd60e0cd-830b-4c41-bde3-bff4f8f20930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 16,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 183,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "38fb0609-0d4c-46e4-b8c5-3a7998ff57b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 68,
                "y": 65
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2f8d2272-ae54-48ff-bb53-da4f5ef3f0b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 147,
                "y": 44
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4681ecd7-ae1b-4678-97e5-a584bdf194ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 233,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7d868f50-42b8-4378-8f27-9c1a8fb8858b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f3fbc0df-2ee9-4357-83d8-39a5436744cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 192,
                "y": 44
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4c0c4d8a-d059-4297-80ad-434623f6a18a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 48,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f9610746-c500-45f3-9365-871f1455b09c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 201,
                "y": 44
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7ce24404-ea06-451b-9a0c-3fb0a818c9a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 230,
                "y": 44
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "13b58cf8-4a46-4d33-9690-9fb3fe2d3c17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 2,
                "shift": 4,
                "w": 1,
                "x": 138,
                "y": 65
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c720c4ba-afd8-4138-b600-bc1477a8c999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 237,
                "y": 44
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1a135f5d-6a90-40d7-981e-ddba83b5bc31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 12,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 210,
                "y": 44
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "7609b7b2-59d6-4de8-8486-b2e74b29de0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 10,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 244,
                "y": 44
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": true,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}