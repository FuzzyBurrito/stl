{
    "id": "eefa866f-d653-469d-8bd1-6ee057e6518f",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_game",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "04b_24",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "13b3947e-cfa4-4e75-ad77-d6289900b9b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 8,
                "y": 79
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "44531ee4-2149-4f14-9f06-2e9f146a3110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 13,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 102,
                "y": 79
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6c240429-82f7-4446-bbf7-2134f48a057d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 7,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 68,
                "y": 79
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4a130d13-be82-49aa-a0ee-a43990332ca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 64
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4e921c6b-2321-4953-ad0b-548a221a7daf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 64
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ec47b910-4c59-42d7-ae10-c4694a604529",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 64
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a470b9d6-946b-4c11-a938-64bbc2652480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 13,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2964e6af-201b-4ca3-b690-635fe86aa24b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 7,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 122,
                "y": 79
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "950d6728-c317-47d6-8672-8b283117e834",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 54,
                "y": 79
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "56e989c7-14be-4a2f-9abd-a97096269bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 36,
                "y": 79
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8ecb1276-528c-41db-ae38-cf6d2171c901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 14,
                "y": 79
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c0c1bbab-2ecd-4b62-8375-af3a8db2e286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 11,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 64
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "62b5d36e-90a0-48bc-a47a-a11d24484176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 79
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "afe9c501-abe2-4b68-8456-f45bc07db79c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 9,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 22,
                "y": 79
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "aa640175-2100-4973-98ba-272fed8f074b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 13,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 98,
                "y": 79
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "70961b3e-697e-4e2a-b954-95fa812a579d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 49
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "896d349c-81a2-4bb0-931b-2fe467fd61b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 34
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0b6d4bd5-a85f-4145-8c76-b4eef3456168",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 42,
                "y": 79
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a23985a1-e4b4-4c8d-9235-f12372764449",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 49
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3ab4a642-f61d-4d35-96e2-7b125cd3fdb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e9b68f55-b1e8-46ed-8d37-4fe8cabaf2c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 19
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "db190d2d-1d68-49d9-af84-a6b5b67e06b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 19
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a1acfc40-e7bc-4cbe-9614-fbc1facc7ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7bd472ce-4d80-40bf-ae1a-c305d91c95a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 19
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "406e36e8-ce31-4807-94fb-13afde639511",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 49
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1addc76b-36b4-4c9e-a5ff-44c2143d8e4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 34
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0ff15ed0-f42c-4122-b5d7-007ce5bb85d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 11,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 106,
                "y": 79
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a032cbc6-df1e-415f-a650-c061d277c4e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 86,
                "y": 79
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "79aede2f-88b7-4b89-a7f5-3548be40e7b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 34
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "89e253a6-82c4-4421-9797-8b48006129b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 11,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 64
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "565baaf9-549b-4217-a0f5-d74249288fa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 19
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f4f954e2-f472-4687-aa32-de3b2972489e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 64
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ec0d20ea-2a5b-4337-aff3-1c5d776a8096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 13,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "083c62a1-a00a-4e49-9163-68dd8242c4ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 34
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "bd48df6a-1112-498a-b3ac-75b5edba77d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 34
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "bf207b3c-4fe3-4001-a403-dd93a00e118b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "520a3931-da1c-41ee-9a54-eb7ca52193b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 64
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f2e758f4-2c2f-4b95-aba6-d3ed8df20795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 64
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d82a5e33-2cb0-4bd9-a39a-f91197c22bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 34
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "08904bef-4123-4eb8-b51e-91f177918914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 34
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b8f2748b-a220-4393-bc28-8703b3575cc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 19
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e32987a2-1494-4a03-9edb-4eb74314aff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 13,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 94,
                "y": 79
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0afffb89-d7de-455d-b038-44e9633ecce4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c5a005b6-c40d-414b-97dc-a801f5d7c5b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 34
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "002802f6-1703-4d20-bd02-9d2e50a85602",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 19
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "05f38eb7-1c0b-4b29-97ba-774fd7bd59f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 49
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "951bdf75-0033-445f-a7ff-f0cc183f1a03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b6f33fb5-7720-4efc-a9e6-d4d6131ce9ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 49
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2af8485b-d4fd-4c45-a97c-b55031b0e8df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 49
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ec24c2cf-4776-48c5-91a5-f1878c460eb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c929bda7-dfdd-43b8-aee1-2c0d3cf9eebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 49
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ae688fa5-848b-445b-a443-7874613df5f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 64
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d596c969-c960-4e6f-8360-b937e20a9c64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "468289f6-e644-4c0a-92ee-30b0ef5c4670",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "307cb4bd-17af-482d-b847-51dc24339572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 64
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b2f03232-7fda-451f-ac6b-444083ef865c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 49
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a3ff0e98-2630-4a51-bf07-4a26940df55e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 19
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9878bc55-2786-4187-af45-36ce23dac5af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 64
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "84a7bfc8-5a82-4d7c-adf1-ab9f4031f843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 64
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9b6d4e8e-3448-4f69-abfd-ec8c6379fcdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 48,
                "y": 79
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3a848de3-c23a-4ea2-b7ab-03ed77ae64ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 64
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1c54c502-bbe1-4a10-9226-44653d741595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 13,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 30,
                "y": 79
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "56505aeb-ad67-42de-b5da-5665c1fe5b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 7,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 60,
                "y": 79
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "09f26a80-9186-4ac5-830c-84b9726c0316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 34
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3fb64b17-b37d-4a76-bcaa-c73faa544696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 7,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 76,
                "y": 79
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d86941c1-dc35-4790-95a4-60a5ed9a68ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 49
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "854db32d-4f99-4e93-a71c-17a88d5a07f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 49
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "50c30bca-aed3-42a0-a4e6-c112c5d39c47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2023d236-0646-432f-852b-39420f2bdf15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 64
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ca3accb8-aa0b-411d-9a0a-ce245e574a15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "27bd02ee-ae1d-423b-b571-ab62598279d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 19
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "acc00a01-28c1-412e-ace6-24a4f9224ca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 19
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9a3a66b1-a64d-46f2-88ec-a78dd24534ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "dfc5796b-8f10-4851-8c7e-25fada0ed3e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 13,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 90,
                "y": 79
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1b5b2b0a-dac1-4d0f-b6d8-c6c236e33115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 19
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "cf76f3cb-500d-488b-93db-88a6a75aced8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 49
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b32da2e4-df8a-4694-a652-8344cbf95bd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 49
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8f095599-ef64-49d5-a445-6abcaba6a60e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 19
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "bb357f36-edbc-4e74-a9f0-2cd1e0db6c33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 34
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "dde3cd99-a022-41db-b347-4af838ae758b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 19
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ec9f8c22-1cd0-4b13-8be1-6c13ef00c01d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 34
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1e43501b-3e64-4d6f-a913-3f2c520fa223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d877c8f4-eada-4013-8fe5-0fd524605e6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 49
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6818701e-de05-4b06-975f-cb02f5c1b944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 64
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0036b78a-4e92-4443-b06f-cdf091d1ba16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 34
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a61014c4-55b7-48e3-a27f-372966814619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 19
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e0a5df0f-d509-4c01-8831-54c85ae3f43a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 19
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "13854c35-f2d2-4338-be63-4f08a8f8523e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 34
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9f6dead2-c6ec-475e-803e-d315d247ca39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d051927f-fe5a-467e-9ac6-702c781407ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "97b02fb3-fb03-4bf2-85ab-403b54bc2a52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 49
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0354675a-f46e-47e3-baaa-3c1b5174754d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 34
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f5bac1ff-3aba-4d5e-856a-c5d7ac218afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 13,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 82,
                "y": 79
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "3f495618-9924-4ce1-97a1-360195dfece6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 49
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "2fd7253d-d666-441f-af59-d1540a9c73da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 5,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 110,
                "y": 79
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "45fa1a8e-45f0-418e-9342-62f4d2eecda5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 4,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 116,
                "y": 79
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": true,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "00000000-0000-0000-0000-000000000000"
}