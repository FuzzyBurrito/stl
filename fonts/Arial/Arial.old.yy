{
    "id": "97de9b95-23f8-496c-98f5-5c6afcc1c049",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "Arial",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0d72568c-10cc-464c-b40b-16bbf09a9166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e30eed7c-f2ba-4ac0-834a-9fa80a54832d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 24,
                "y": 77
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b0565c5e-47f1-47c0-b93b-5e63c22f7eec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 15,
                "y": 77
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "740d7521-822d-48df-801c-f17233fbd15b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ff0d2312-321a-4f39-8e60-17e2e3ac29a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 241,
                "y": 52
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "356a8926-364a-4b9a-9284-0f2497d2272d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 23,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 223,
                "y": 52
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "06511739-5356-4873-bf55-5512317362c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 208,
                "y": 52
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "fb6d948e-318a-4480-b9e8-55cff28a0fb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 23,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 203,
                "y": 52
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b82578bb-b471-4e3a-947c-1db40926071d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 196,
                "y": 52
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "568bc740-a6bc-4e19-b81b-28e74c12cafd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 189,
                "y": 52
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4c581126-56f1-478c-935e-bd7ed2641ff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 23,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 29,
                "y": 77
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "19e49ffd-ec98-4ce7-8afe-0f1622afbbfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 177,
                "y": 52
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1da30f0b-cda1-4acf-92b0-51fdbac6091a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 159,
                "y": 52
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ae0dba1b-9bd1-4d3e-bad3-958512ba61b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 150,
                "y": 52
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "31729fa1-a21b-47b6-bd47-80db4b0375dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 145,
                "y": 52
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f60822b3-cbd3-43cd-8b21-1b20b1bb276b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 137,
                "y": 52
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1e093755-5ee3-4596-9033-df540520c549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 124,
                "y": 52
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "3bcef102-b5d7-4550-9d01-951d16f66c60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 116,
                "y": 52
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3be1729f-b204-4fa2-878f-e583211fac26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 103,
                "y": 52
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0cc8ca7c-fed5-4946-b2d9-b02df150b271",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 90,
                "y": 52
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "22b66a08-4a74-4d36-9008-b3d9e6b09583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 77,
                "y": 52
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1b178da7-bccf-4489-8599-2898af967780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 164,
                "y": 52
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e61e4ecd-ea1f-4ba3-8f74-bfa380b91007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 39,
                "y": 77
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "4ac920d8-52b5-490f-b717-fd7e0b27d0fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 52,
                "y": 77
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "534245b6-a299-4f31-8f88-0f9a476494c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 65,
                "y": 77
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ebdbaf78-841c-4350-8cf5-7d8fce803a7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 99,
                "y": 102
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "16837b76-9786-435e-bc5c-ed701fd753af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 94,
                "y": 102
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e448af45-c77a-40b3-98f5-2828174cb0ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 89,
                "y": 102
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f390d386-7315-407c-81ae-06c0f8291599",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 77,
                "y": 102
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "67335e63-47a2-4483-880c-13e1b94d1457",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 65,
                "y": 102
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "824ed4fb-0a30-4f14-841b-9c1597930340",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 53,
                "y": 102
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "727a2a9f-8839-4830-a9fa-5bef34d94689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 40,
                "y": 102
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6c1c03e5-738b-47d2-985c-8bc7cb661874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 23,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 19,
                "y": 102
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "03a74c11-501b-456c-ba12-2483e0b629d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 23,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0affa0ec-3e7e-4c8d-8b9c-549988f3ff28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 225,
                "y": 77
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "52c17202-2ccd-4c6e-afb4-80840434b8f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 210,
                "y": 77
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2686ca24-cc1d-4ece-9dc5-6ecb4859aae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 195,
                "y": 77
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a5069dea-42d5-40c8-8369-2f0a7a52f270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 181,
                "y": 77
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "86b89369-2b3c-40a8-9c40-e8094739f525",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 23,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 168,
                "y": 77
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6277b0e4-351f-4962-acd3-481dc2ac80e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 23,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 152,
                "y": 77
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a993f26f-3f5d-43e7-944e-c6d19d2c3309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 138,
                "y": 77
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4b6ecb7a-54da-4f2a-b74c-675566dc105c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 133,
                "y": 77
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6fc1402d-874d-4bb5-bcb7-a646bfb02e51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 122,
                "y": 77
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9c22a962-5ae9-4d05-bdb4-220b5c45f7a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 107,
                "y": 77
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "36559206-dc7c-4d54-a48c-9661e241f1e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 95,
                "y": 77
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "839333a7-13cb-460b-8eab-6cadaf74265b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 23,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 78,
                "y": 77
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "aea47757-9726-4a57-81c2-2d4441e6b2c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 63,
                "y": 52
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a7d8b747-c922-410f-b60b-38dd132c467f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 23,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 46,
                "y": 52
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "71265c6d-bb10-4059-8b42-3e9b094d9885",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 23,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 32,
                "y": 52
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "54c76d8d-ce24-4d68-9c63-3fb0fb86f954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 23,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 25,
                "y": 27
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0a8eb3a8-9aee-47d7-8a85-0f1d9d9ea922",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 27
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "dc190a32-4a13-4401-b877-97d671ca1156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "eb0db417-f169-412b-9b8b-7f19329b34a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3c499bad-2a87-48cc-94e8-3b953022cc62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 23,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2646fef7-dcfa-495e-9a25-f5c39b685145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ffdb9138-bae5-4d1c-b8dc-23ebc5a1e2ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 23,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "20e7c8fc-2f78-4366-b94d-a70bc0735296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6fc1f5cc-7816-40bc-acbd-d396769a705b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 23,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8534aa87-0412-43e6-af0c-4320978ebe72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "bd4ad56d-e01a-41c8-abf6-d71127f01c73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 23,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 18,
                "y": 27
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "cd14e809-ffa7-4808-a1af-6c6142a6530b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "85a8614f-b312-40f4-8e75-d248b2535ea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "44cadf16-6984-463a-971f-e28b16c3bc83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 23,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "aa5da4f7-78eb-4e6e-af21-7a91f0e3cd52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "74fb4d6d-ac37-4e7f-9828-36d711b4f20c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3f8daa5f-9fa0-4b2d-bafe-1aa7505bf615",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "61defdd4-3f58-4f5b-924e-5f0209f7be8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "bf38a755-e865-4939-9cec-cd7ef9f4c534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "51181a38-19ca-46e1-bd1a-686ae84b1a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7dd25c9a-f0b5-48f7-aaa5-7270f1c03a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "158728c0-be4b-4822-8ca5-e7fb68ecfc9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6ce173f3-cabe-4cb8-9f83-94e902235ab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 42,
                "y": 27
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3ad93174-7c37-4ce6-abaf-bd6c643f33a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 158,
                "y": 27
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9f31615e-aa47-446d-b65f-6cf36cd3a84b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 23,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 54,
                "y": 27
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "06238094-424d-4ac1-b033-41eca9fc5e66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 23,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 13,
                "y": 52
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "446d4b7d-8333-4010-aa0a-f02adfe3e380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 23,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "ea912206-eee2-4f62-935c-c5ffef5edba1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 23,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 250,
                "y": 27
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6d764889-e3d3-490d-81da-99ef76bd4a10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 23,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 233,
                "y": 27
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "88355be7-b6c0-4b72-bce3-52bcfa68f02a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 222,
                "y": 27
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "473dda54-8eea-4e01-8375-14c3484368bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 209,
                "y": 27
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "16d30bdc-dea9-4e6c-9ad2-227c24b40b05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 197,
                "y": 27
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6dc80b09-4344-4ab4-baa2-f1b659855230",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 185,
                "y": 27
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6e1a88f7-819e-4116-8c44-bd4aff80494d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 23,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 177,
                "y": 27
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "aeae1f10-0028-402e-b746-1e8cfce355e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 20,
                "y": 52
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3fad9b8d-c0fa-4362-a915-d305af9f599f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 23,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 169,
                "y": 27
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d7a83109-8267-4b49-a7f8-d69c892ad2f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 147,
                "y": 27
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a6b6ec83-bf97-40bf-bbc1-1a8543216e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 135,
                "y": 27
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a56a532c-305a-49f1-bd28-7d047d7a8815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 23,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 118,
                "y": 27
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "92417493-a592-4595-bef8-c472f8e9fac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 106,
                "y": 27
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d78149aa-e851-4af8-bba6-a4763c624c55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 94,
                "y": 27
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "dec8bda0-4f77-4a81-9561-3ca55329e4c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 23,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 82,
                "y": 27
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "115c6bf9-8e79-42b2-977e-a975e5ff73b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 73,
                "y": 27
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "36c4a917-a8be-43a9-9380-861fb37b6798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 23,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 68,
                "y": 27
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4d741cce-2a0f-415a-99dc-8b4a0d623ad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 23,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 59,
                "y": 27
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "bbd76be6-6e65-46de-9e63-04fcce7c02e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 23,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 112,
                "y": 102
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b8bb5a92-171b-4189-b224-2605b8d49a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 23,
                "offset": 4,
                "shift": 19,
                "w": 12,
                "x": 125,
                "y": 102
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "68883073-9d04-4068-bb78-3771ee6e9eb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "fc231116-0a8c-492b-a026-d9721b4b33cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "abdbcc49-4eaa-4f56-a75e-2aedace9d1e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "429330dd-8407-4873-9a28-053aadef50ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "c077c273-465b-4bb6-9509-966c5baeb021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "6a128c5f-6f8b-4d64-b49c-e982c021ea4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "6be18370-5e35-4edd-93a1-d4bd1d98708e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "d7d1039f-7876-4d3e-9134-c6350122dc3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "7b2e137b-78cf-4076-ac60-06b3a0f34b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "a76c7cb0-20a7-406f-a0e1-8d685c66a0da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "9fbf52ad-853a-4c03-9f88-ed5feece8631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "7f03b4b1-6ade-4cf3-b502-8a903a99dbb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "b6987ad3-03e0-4109-a671-c1fca8e02c64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "5365ef8c-3ef4-4e7c-aed9-4e175366d34a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "9403bf18-68a7-4193-aa2e-fb19886df865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "784a8b20-38a3-4dd0-9af6-b11ce4004ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "4cccce29-42a6-4d9c-ad61-5ea2cdce6a11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "c825eb59-1e14-4d49-9f70-4616d5e6735e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "bcc2db65-9235-4bd8-909f-a84b78911e63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "ea31e758-c676-4c5f-afa6-33d3a8102a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "c518e793-7e4e-4e85-afff-cbe02903f7df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "55d7a076-1e05-45e2-a573-09e0c58285a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "782858ef-ef5f-4827-9ca5-41a613dd2d5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "6960ab59-a8bf-4cfd-aa2a-dfac2852932c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "66472ddb-c1c1-4978-a967-b7874ed9b631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "fa297cd3-e514-40c9-8a41-4078be0e73b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "717e6df3-7d95-434e-ae94-9b7649a06576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "dbfda212-32f6-4a34-8f7b-aca6c3b0cb02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "03fe518d-6336-4e59-92cc-916e17697f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "902721d9-1afc-46a8-830c-1ce7ffd0d178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "4c09db81-a2a3-4228-acaa-bdb0fd524d99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "8e5d8421-79f6-40b9-8fc1-3f6253000066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "8ed2cae9-eadd-47e9-a79a-d653c669d883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "392f5dc7-c28d-4df7-960f-4726acc2cb8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "6f4a4d27-889d-4a36-bd48-8193920b2da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "1a54abd4-70ab-4b96-8d8c-f41d01a52bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "ee889b36-10f0-41da-b545-b975d0f77909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "0261ddb0-5d82-45df-b676-2e7ae6fa616d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "a8804641-9fbb-429a-84d1-c9f970efcd08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "4142c555-2b86-446d-ac01-765462d9ea52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "c3c46645-3e9d-47f4-97bc-aa105cb72159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "0a970788-72f8-497f-921f-2907a0f31121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "32b4fb5e-8bbb-4242-a9e8-e7342d6064d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "b915a292-8d9a-4421-bd10-c2db05d2024f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "64b50c39-5b6f-4000-84ac-6edcb56a4eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "3f76e94c-e042-49c9-af6f-5838063d3f6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "825e1066-0fa2-4d20-9255-c55fe90386bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "2ff5f9b9-b175-4efc-8bb5-93b1d140401c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "20b7ca58-6d2e-4fe5-a5d9-e804c4541853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "49733e24-06e6-49af-8ab4-a5260806d7c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "2239f586-423a-4910-bd76-9b0ec65563e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "89621cab-7649-4860-9517-1d2f185a1e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "b4c5a775-60bb-4a38-93b7-a750e94e361f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "b794f6b3-90b6-4e1f-8256-9fdf20316f61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "1cc34859-8764-4761-b804-8d6f0ce4195b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "f81c90f4-4b4e-4adb-97c8-68018c0e1449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "f50d85de-3076-4bac-9254-18225abf0840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "4a2de6c9-685b-414f-a68f-05dbdcfcf6fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "bb168475-1787-4b5c-bb69-a4cffad29d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "695a02d7-d330-4e3e-8e23-a1bc155eb617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "5523c1c9-491e-4436-8307-1c28f07efe0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "09749a0e-6cd6-422d-9831-64d918a14ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "19a11dbb-d003-4a60-8bca-31dcef863aa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "a3d7c851-ecbb-4322-9191-83acdc144f1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "30735a61-9c3f-4248-8929-3366662b1170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "4ce5c18d-9915-4c00-8445-b1a702122831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "a7b4de44-5d0d-4a92-a987-84bac9aac667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "8baa4c47-0f0e-4011-9b98-fe0178b233af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "69358651-6088-48e8-906e-43294378b635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "1be934ca-4fd1-41e9-b999-7b26ce125782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "9884ee09-630d-4ad6-aab0-1955c6ff1f0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "2777e09b-3bba-4279-9faf-90b1dbf8479f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "c69582cc-6d55-465f-a4b1-9fa85ce64a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "633cd6ef-6903-4dcc-b07c-46d57924b820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "6c799012-44d2-423b-b4b3-37efba5674ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "d822311f-1e95-4465-9c75-ac578e7e186b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "05c678bb-9cf5-4502-bdfe-219fa3e9472c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "f8a75b16-499f-482c-809d-85fa8e7ef0f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "659126ed-6ca5-43a1-b5a4-22f7a9e3e138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "bbe1bc17-4f05-41e0-b032-a5c19d0fc05e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "b3f33806-a1dc-418e-8b07-65afa48fbd82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "76a6a9ab-f117-420e-a0d1-7701e0fa1251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "a96a8640-999a-47e2-9f2a-455053af0f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "abcc2109-b2e0-49ce-8078-04796e85c30a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "d58f77f5-20c1-4bce-b94e-3d11b2b53578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "17809ef7-25cd-460d-87f9-4bef892e3354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "f7c04739-c429-43c5-aea8-3ee5fc430fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "caf91103-34f4-472b-94eb-4c4724c0e6d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 15,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}