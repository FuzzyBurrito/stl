{
    "id": "97de9b95-23f8-496c-98f5-5c6afcc1c049",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "Arial",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ee75b74f-472b-4300-8624-50f4298b1b93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "218a08ab-01d9-404e-804c-86675c9cd26f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 367,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e687273d-9c0d-4852-931d-52eedc571ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 46,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 353,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a8813fd7-e2a7-4efb-bb38-323767c072e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 329,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "25c5993a-860e-49b8-9907-5aa27983763a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 307,
                "y": 98
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6815c2a4-3ba8-4b7f-98c9-baab9a91bf26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 46,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 273,
                "y": 98
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5e3f70e4-da20-49ea-a272-28e789f13890",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 46,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 246,
                "y": 98
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "dec9edfd-3b15-477d-bc2e-b5ef2920017b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 46,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 239,
                "y": 98
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "672ccf2c-3f7c-448f-bad7-d57f05adbcb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 46,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 227,
                "y": 98
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "80966458-459e-4912-be39-46c4974c9b86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 46,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 215,
                "y": 98
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "09055fdc-79fb-4971-8a49-ac615ccd6b25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 46,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 374,
                "y": 98
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "428220ab-c847-4d78-a781-d3e18f72aff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 193,
                "y": 98
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8eb2dc48-12c4-4a78-bc59-fa015c9617e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 164,
                "y": 98
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0bb688ff-4e0e-46b0-ba71-f7caa1dca075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 150,
                "y": 98
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "abb3c6fc-d791-4835-a35a-28ccbfb541b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 143,
                "y": 98
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "3699202e-f56a-419b-8e85-68c64c0ef23b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 129,
                "y": 98
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "864f8787-3e4f-49f7-9ecb-9d9620d48341",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 107,
                "y": 98
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f54ee9b9-b154-45ef-9181-268bc508fe43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 46,
                "offset": 4,
                "shift": 22,
                "w": 11,
                "x": 94,
                "y": 98
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c00a3e98-ec63-4c94-854c-1867d913f1f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 72,
                "y": 98
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7a8f2256-5aee-4247-8515-d857c11f2efb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 50,
                "y": 98
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ed4ad900-ae9f-4042-8da5-9a393c2acb86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 46,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 27,
                "y": 98
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "da066cba-479d-43fa-a2d1-210a8c8571d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 171,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "de9e3337-e849-4c63-bd51-0fe2af74c38a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 390,
                "y": 98
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "34786a27-90a9-4ff6-b644-bd516b09a671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 412,
                "y": 98
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "5c3a5e3c-89d5-4672-b9f1-a2c1caded202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 434,
                "y": 98
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "361ca4f5-503d-4eb6-ae4d-1ca5154bf545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 405,
                "y": 146
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3db5c93b-e16c-4073-8547-c0c6f7e7a3ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 398,
                "y": 146
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b7e431d8-6576-4d5d-93e1-6da7645fc29a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 391,
                "y": 146
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a698706d-fdc3-4f90-a096-5b81cfabeb01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 369,
                "y": 146
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c2ff96dd-956c-4ec4-80e4-11c2fe90d86a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 347,
                "y": 146
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "343204d5-3de5-4bf5-9733-ed457b2ef7aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 46,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 325,
                "y": 146
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "da802f09-6f30-4bd5-9aca-433f540ba422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 303,
                "y": 146
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "05635605-a0bf-409e-9772-7c9c35236d61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 46,
                "offset": 2,
                "shift": 41,
                "w": 38,
                "x": 263,
                "y": 146
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e5ed79f3-0305-4bf1-8c8e-2e6a5f55c4bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 46,
                "offset": -1,
                "shift": 27,
                "w": 28,
                "x": 233,
                "y": 146
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "247a81cb-d030-442f-b113-a7e5cef017d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 46,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 208,
                "y": 146
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "d8d526b8-644b-4925-820c-943c4cc9c4a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 46,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 180,
                "y": 146
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "19b0d9c7-cac0-471a-ac0f-3cddc866f9e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 154,
                "y": 146
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "baf098e6-39a8-40c9-8032-2817c13a73be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 130,
                "y": 146
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5bb5cf7c-9657-4a7c-87c2-4d70fc3fd64c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 46,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 108,
                "y": 146
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b0340658-8249-4c5f-b61a-5265a6d924c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 46,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 79,
                "y": 146
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "87da283c-19b7-4dd9-b139-1585f2cb5cb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 54,
                "y": 146
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "81131f48-0e54-476a-9b7b-59ba9c35ec7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 46,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 47,
                "y": 146
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "582aa238-9290-4559-919e-2938b0688408",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 16,
                "x": 29,
                "y": 146
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "617912c3-c122-4901-b668-4cbb5e23ce10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 46,
                "offset": 2,
                "shift": 27,
                "w": 25,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a3f9d785-251f-4900-8437-3cfec0e72fac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 487,
                "y": 98
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d93efd1d-0461-45e3-8ccd-f9f61c8b720b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 46,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 456,
                "y": 98
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "19a04421-28b1-4606-be0f-872b229541e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "071cce0d-b016-4943-a814-a1ca2bd814fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 46,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 479,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "66e28963-dd09-4f61-8ae3-2c174a46425f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 46,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 455,
                "y": 50
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "1f025bd4-4209-4931-b462-10bdbc2bcd61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 46,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1f24651e-207c-4813-a653-fe57a08a3bbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 26,
                "x": 448,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ded0ba18-fe3d-4edd-adab-991d428d7dd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 46,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2c9e0ace-b605-413e-ba2f-6420f589bd9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 46,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 396,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "390e321d-0be5-4862-a00d-b9509c313ed0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 46,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 371,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "be6495c5-6bc0-4196-aeb3-1dd5e8b31459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f083f288-edcd-4ac9-83c3-c7a934610762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 46,
                "offset": 0,
                "shift": 38,
                "w": 38,
                "x": 302,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d6b803c3-1ac6-4464-b991-7df7ecfc15cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 273,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "937b6607-df1e-40a5-ab90-e610ef3db193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 46,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9b51a939-bfde-42d4-a5ea-82f8d8314d26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 46,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "fc9ee88c-de2e-4f08-82b6-5784d20f83dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 46,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 476,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2204e2f0-dbc3-4c2a-aafc-6d3fda115512",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "295c94bb-4ee8-44c1-9d6a-b6f345b04425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "858d419a-65d4-4ae2-9413-731cf3bf8820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 46,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9770cf21-eeae-4f87-8427-1b7e20c813d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 46,
                "offset": -1,
                "shift": 22,
                "w": 24,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "96d8f9e5-31fc-409e-a465-1d835ae82cfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 9,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4af99bf0-412d-4565-8b26-158bb98e55b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ffee5ad8-2953-46a5-984f-d14b044f7734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "31f6b2b9-a188-46ae-a86b-17dcb124d72c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "223d7c96-f4df-43e3-aeff-01320c84ba17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "267566e6-a842-44f7-b788-24e003c47b76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8a6180a0-2721-46c9-9022-86a86644143a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 13,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2c7cf38e-7703-45cc-87bf-5e6d84837ad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 33,
                "y": 50
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5c1ac144-76c2-4487-9963-35a53c974b66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 235,
                "y": 50
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "fdca661b-3a03-4914-8fa7-737e2cb05cd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 54,
                "y": 50
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "66c571ae-5511-4cd0-9dc4-f088faf27dc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 46,
                "offset": -2,
                "shift": 9,
                "w": 9,
                "x": 424,
                "y": 50
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "811f0480-a4b2-436a-8a78-596e226ce0b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 46,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 404,
                "y": 50
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "fb57e7a5-07bf-4f3a-af91-675b7d8dd954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 46,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 397,
                "y": 50
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "da14c411-ed81-4cf9-abdc-1f488174499a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 46,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 366,
                "y": 50
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ad770bab-ef50-4865-8049-674a89d35d4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 346,
                "y": 50
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1576a334-0dbb-4ff5-9cc4-7b3ce490b3c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 324,
                "y": 50
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a35a4bc7-2c3d-449d-8e33-096142a40ed0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 303,
                "y": 50
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "46081763-026a-4ce9-9035-49a89aedb09d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 46,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 282,
                "y": 50
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c6cb9cb6-8363-4847-a096-20a89e8361d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 46,
                "offset": 2,
                "shift": 13,
                "w": 12,
                "x": 268,
                "y": 50
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "42d94c11-ec44-4053-9af0-4ac102b5ce58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 46,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 435,
                "y": 50
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "12c95664-9d81-478e-8fd9-e926b29cff78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 46,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 255,
                "y": 50
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a597ad08-2b27-477d-b924-8f6575e03c42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 46,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 215,
                "y": 50
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1c4798c2-6fe1-4608-a1d4-9655a64aa5e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 193,
                "y": 50
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "bf25dd86-61b8-43e7-a786-b3544b8cc5df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 46,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 162,
                "y": 50
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f5e01c3e-672a-4cac-95c2-ef008eb4436f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 140,
                "y": 50
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2306fea2-ff99-4be6-88c9-9a73a890991e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 118,
                "y": 50
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a7d1259b-7183-452b-9dc8-59724caec5ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 46,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 96,
                "y": 50
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "37ba95e2-5900-4bfd-a819-cbc916ad2dca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 46,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 82,
                "y": 50
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "78ef60d3-98d9-498f-ada1-7cbbb5c7333d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 46,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 76,
                "y": 50
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7caa8f52-f7e5-4b6f-b68d-33f3f866c12e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 46,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 61,
                "y": 50
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4c7a0b90-f3bc-489a-8f31-4b2ec5dec705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 46,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 427,
                "y": 146
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b0ac84e9-f4d4-4001-8bdc-7f0508560f0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 46,
                "offset": 8,
                "shift": 39,
                "w": 23,
                "x": 450,
                "y": 146
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "77a30fd5-8f6e-42f9-9428-c4b9961a5bf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "660e278b-c7ef-4114-990a-b703730840c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "c491a940-255e-4daf-bda3-56c6d9bafd2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "ba574964-41b2-423b-a7d9-15988d89657e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 902
        },
        {
            "id": "6dec1431-accf-4ec2-b486-7f399b85311f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 913
        },
        {
            "id": "f9c3b710-f414-4d6a-bd1d-b8d7501fb7e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "89f6e293-4a09-498b-ba93-a64dd55496ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 923
        },
        {
            "id": "44ae04b6-b132-4e7f-bb04-c3be0b37b64a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "7f7fb906-70e4-4b66-b621-1ef885b9f8ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "8e59589f-1dce-4282-9214-3d3e4034d73e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "65a210d9-5fef-4e5d-b9ec-eb7cd927dd59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 49,
            "second": 49
        },
        {
            "id": "5f85f9c7-8435-4635-a6ed-a06d56977bd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "382cc1c0-f8d0-483c-ad01-1f72153d8a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "76f74e6e-18a9-416f-a9ef-e999fc9f0faa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "d00f977b-9a2b-492d-8f03-24070289f3e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "f0761901-ee85-4559-96c8-cb6ed2a558c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "4ddb5d80-20a0-4d25-9735-b0ff70b25553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "cdaa8d4e-52c3-4ce6-bdab-bc4e5310a57f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "3f953953-450a-4155-9ee5-564244809450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "cb6af17f-94e2-4c7d-b1c4-05a8c2f455f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 160
        },
        {
            "id": "f8cab952-9d28-4deb-aaf4-c716ae8b0481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "767d99bc-a498-4a66-8891-8e5ccb8e57f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "58fc219c-09d1-4c31-b4bb-5e44efff06e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "39cca40c-6c32-4d6d-88ac-37280bcfc37d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "13277a12-f7c3-4a3d-b55c-9ad511256a52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "c969d229-9ec9-4e4c-a52f-f298860176f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "00c8023a-1034-488b-a56b-954e017e21fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "56f7a4a2-8ca0-4309-9f0f-3089fb62aab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 87
        },
        {
            "id": "fa3b7227-fe57-48ef-88bd-cbbaf723c3c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "e18b5210-f384-43bc-b058-ecd681695609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "8963790b-87fb-4d92-8199-ce42a4e1c265",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "e1b2399e-bf00-47d2-8b3e-331e3a85d1ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "fa57c39c-8727-4a11-9c22-5e8d3bef7b97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "0b0bbfd1-766b-4129-896a-7deb41a129c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "5965d9ef-f351-446f-b3b8-730fd5975bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "efaf7c41-398a-4e49-88e8-29eb71400424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "c79c4510-a188-4031-929e-55c0107454b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "3f78f942-eb92-4ff4-bce6-5888e14869f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "dfdbf5ea-e241-4858-80c4-c250aafdd303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "ece403d2-e788-471c-b90b-bc445ae54fdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "5cd742df-80f5-4944-96b4-36e5966120ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "fd65934f-f1ef-4780-900e-f1279a946b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "6e317257-395c-4860-8b14-be1676363d0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "3e8f770e-7726-42d9-a810-30b35a85a924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "e4d74ac6-eed6-47f5-9ad6-b024b78b9069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "e5282920-05d1-458b-ba36-796066750431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 58
        },
        {
            "id": "7499d591-6194-4669-833f-e368926e3312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 59
        },
        {
            "id": "74ac8a44-f7b3-4b78-bae7-808cfbfa1d3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "610f9ec7-470e-459f-945d-7bb96da0ad7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "763c8e77-48b8-42ae-8e62-4f3cff3cb537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "131c2682-c464-4ca6-92eb-6e5433fd2dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "6fc53296-4910-4aa9-9b44-9b3755a9cfff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "f3251526-1f30-4970-a503-c5386d747687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "c210c86a-790f-4bfc-bc28-330a4b3f575c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "e9beea1d-331f-46af-97cd-2e5dc98e21f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "24b7209f-7896-4318-b5b8-736bd717f134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "da507d02-ecf1-4ce3-b16a-4cdc120abdad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "cb565f54-c36e-46da-b4e4-b34451a93337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "00816a56-dded-4f5f-8053-732dd0d5ea0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "3fa28f36-2faa-4e74-bc26-9e429061d278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "0bdad3df-4abe-4a2b-bd95-5956f6173b91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "881cf30b-2ca0-4964-aea4-fde528863dd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 894
        },
        {
            "id": "47449f5e-38e8-447b-84c3-6a1013f8d651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 44
        },
        {
            "id": "5088dd9b-b1cb-48df-ad20-40a0a4922d3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "2096a04c-1b78-48bf-9002-6c6bb37ff680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 46
        },
        {
            "id": "ca13461b-f921-4804-8414-2e55f027c0bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "5cf2736b-1072-4d87-952f-aad9d5c2fc3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "1675acc7-08b4-4108-b7b2-43817d8fb9e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "fea5d62c-0633-45ba-b8d6-4bbfadf4a417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 97
        },
        {
            "id": "0816dd57-823d-4063-b4d3-92fa2e553094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "45cdaa11-a131-49a0-8616-b6bfb35d9406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "c125536b-f93a-4df9-9886-d4747dc54647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "b2e29613-a33a-4626-95cc-cb0a3656abed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "7206d7ac-5ac5-4279-9e3b-1b7d8e180577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "15dc23aa-2452-4c0d-9a7f-f62db7ab2453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "b0a28a55-d638-4c2d-80bd-2c7ea1de8d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 173
        },
        {
            "id": "78268688-49a9-40f7-bcd9-caceee8e85f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "aa79dbb6-e001-4497-b4a3-e5c10dc93540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "1d4cc26b-b980-4542-a3e5-411021cf2e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "68bc34e4-b443-4dc5-a277-ca9f71b7218a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "1aa7184c-3ea7-4d41-82dc-3ba0ccbbed17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "8edd6428-cb63-4144-b16c-35ea2bd4bfb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "0ac3110c-c76d-41db-813c-e2bb074a8b43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "dec34cc8-d423-4499-8b41-8348518b35e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "6f434899-2c09-46bb-a978-32c4eb8c7e5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "8091665c-6c42-4300-b69f-41a139bad8bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "c9a6d00c-d6cd-411b-a675-dd5184109499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "f17a8b8f-e0f3-4664-9881-65a79f821fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "0ceb6c9a-8245-42ca-87b2-2a307d81ca7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "463408fc-7b08-4735-98ec-3758d9d0ae7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "2a790904-c0db-463e-9a21-d5cdb88831f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "b284fe8c-ba35-40b7-b623-39423f2dfec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 44
        },
        {
            "id": "260cf130-d2e5-412c-9025-39b44d4d85af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 45
        },
        {
            "id": "a4659cda-d3c5-4017-8fd7-45e82bd146cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 46
        },
        {
            "id": "1b4a6be3-e748-49ec-a82e-14c8763ba9a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "761cd63d-3172-4afe-a80a-74d9914bd939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "6a67f7d6-8594-493d-8ec0-aa32e1fb800d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 65
        },
        {
            "id": "8be317a5-b9a2-4e4f-90b9-aae350232e89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "daa6408e-69c6-4cae-8d2f-158ebafde5dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 101
        },
        {
            "id": "97c89377-80c6-4495-ad20-24ffbc05626c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "706fef1c-ab75-4a95-83ad-a296ea487948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 111
        },
        {
            "id": "c23c84ea-8fec-4fc4-8f0f-2c37d3064dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 112
        },
        {
            "id": "5ae7ee91-2dc3-4d1c-b8b7-f30f25160e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 113
        },
        {
            "id": "214cc31f-2e0b-4905-bde7-bf166a19c609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "664f7b2b-50d6-4fc8-a15a-86ec2a66628a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "87fef78a-06f4-46fd-bb32-97df0e3e6845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "22b4a521-5e63-49e3-bdcf-8c3c785f0935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 173
        },
        {
            "id": "b03c28c4-a6a7-403d-ad51-b13abad7aec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 894
        },
        {
            "id": "782dc75d-fcd4-42b1-8e29-d45a21996b33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "47c48e99-1f6c-4b87-a6d3-f1a6c5866b2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "073fad50-40ca-47e0-8b2f-1fab9669d38e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "a3ba7d10-a370-4431-b9c1-b0470e4ed20f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "9d46b88c-7d2b-4ffa-bdbd-f67b9b84309f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "e1b37271-3f7b-4770-a465-1566ceffbf8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "4715923b-2764-44af-a3d0-a8d29a605668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "29f52eed-acf9-4a9e-bc60-c8a695791b6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "178c9ade-6d95-4701-9e2b-55ab76d747f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "f986a437-aad7-4e3f-a57b-cb5550975768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 44
        },
        {
            "id": "9be637a9-b788-4732-bc1f-a76bb2cf560b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}