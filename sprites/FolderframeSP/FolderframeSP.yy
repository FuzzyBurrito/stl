{
    "id": "72aa6193-c682-4b12-b211-398da4db5863",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "FolderframeSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 0,
    "bbox_right": 81,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d2cb84d-d45a-442f-8e0f-b561c2de5eca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72aa6193-c682-4b12-b211-398da4db5863",
            "compositeImage": {
                "id": "26ffa9e2-69a0-4811-b462-85b108eafb51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d2cb84d-d45a-442f-8e0f-b561c2de5eca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb338406-8e34-47a6-b1f2-d6921f5fb073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d2cb84d-d45a-442f-8e0f-b561c2de5eca",
                    "LayerId": "8273e46c-c602-4ab2-87b7-c418832eeed0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "8273e46c-c602-4ab2-87b7-c418832eeed0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72aa6193-c682-4b12-b211-398da4db5863",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 82,
    "xorig": 41,
    "yorig": 25
}