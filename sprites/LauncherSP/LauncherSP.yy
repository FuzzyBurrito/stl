{
    "id": "070ebd00-81ff-4ddc-8290-86d44ba8ae02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "LauncherSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 3,
    "bbox_right": 295,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79cbeaef-be37-4ba8-af7f-6b7286f81e0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "070ebd00-81ff-4ddc-8290-86d44ba8ae02",
            "compositeImage": {
                "id": "7444b150-b8a4-4eed-9ca0-41364672c29b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79cbeaef-be37-4ba8-af7f-6b7286f81e0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae3a22be-e53f-42b9-ade1-4c2f2d922337",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79cbeaef-be37-4ba8-af7f-6b7286f81e0e",
                    "LayerId": "42e5d72c-e54c-473e-bc6f-460dde9b939f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 43,
    "layers": [
        {
            "id": "42e5d72c-e54c-473e-bc6f-460dde9b939f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "070ebd00-81ff-4ddc-8290-86d44ba8ae02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 21
}