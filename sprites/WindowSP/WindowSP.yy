{
    "id": "aa08edd9-9183-4f46-b8ac-6ba112719e8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "WindowSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 505,
    "bbox_left": 15,
    "bbox_right": 887,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40c1f540-7950-49d2-a0ee-ad6d27834783",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa08edd9-9183-4f46-b8ac-6ba112719e8c",
            "compositeImage": {
                "id": "d3c15c46-65cf-40b6-b31b-09f07e57a8ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40c1f540-7950-49d2-a0ee-ad6d27834783",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c064be31-a1b0-4585-82f6-bf367468684b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40c1f540-7950-49d2-a0ee-ad6d27834783",
                    "LayerId": "d9e8d6ad-21b1-4552-9309-0f0756a4fd43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 518,
    "layers": [
        {
            "id": "d9e8d6ad-21b1-4552-9309-0f0756a4fd43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa08edd9-9183-4f46-b8ac-6ba112719e8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 259
}