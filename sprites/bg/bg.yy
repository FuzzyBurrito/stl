{
    "id": "660e34ef-e17d-4faa-90a6-917589ef151b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35094e6e-f9cc-438b-a1a2-2c044b408d53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "660e34ef-e17d-4faa-90a6-917589ef151b",
            "compositeImage": {
                "id": "091a60ed-8e46-4a82-a251-ef24570c0ca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35094e6e-f9cc-438b-a1a2-2c044b408d53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b098d5c6-271a-4aad-8124-0de4f8d3e130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35094e6e-f9cc-438b-a1a2-2c044b408d53",
                    "LayerId": "8d203c41-6adc-43f1-83c4-e2b9b9b0bc73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "8d203c41-6adc-43f1-83c4-e2b9b9b0bc73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "660e34ef-e17d-4faa-90a6-917589ef151b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}