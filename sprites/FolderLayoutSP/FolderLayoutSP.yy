{
    "id": "7321efe8-fdb5-405e-af0b-7336d88fbbac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "FolderLayoutSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 0,
    "bbox_right": 81,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b080d18a-f825-43c7-984f-ac927203dedf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7321efe8-fdb5-405e-af0b-7336d88fbbac",
            "compositeImage": {
                "id": "bddf01b9-2d2d-4160-8409-9b6d65b443c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b080d18a-f825-43c7-984f-ac927203dedf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f60285cf-b0e9-4af3-8997-44caddeaf9c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b080d18a-f825-43c7-984f-ac927203dedf",
                    "LayerId": "0857a847-cb09-44e9-98af-70f86a1ab522"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "0857a847-cb09-44e9-98af-70f86a1ab522",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7321efe8-fdb5-405e-af0b-7336d88fbbac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 82,
    "xorig": 41,
    "yorig": 25
}