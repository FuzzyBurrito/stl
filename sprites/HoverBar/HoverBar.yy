{
    "id": "9fc4850e-1718-4325-86ed-810f61dc6f9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "HoverBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 402,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7acf5e53-d771-4e9a-820f-36f3a8dd2867",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fc4850e-1718-4325-86ed-810f61dc6f9b",
            "compositeImage": {
                "id": "15409f89-d279-4c59-a4e5-66b588161944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7acf5e53-d771-4e9a-820f-36f3a8dd2867",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f4aa4a9-d3dd-4ffb-9415-7800cf812f9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7acf5e53-d771-4e9a-820f-36f3a8dd2867",
                    "LayerId": "0d574e61-dd63-44c7-a6eb-e8ad63429978"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "0d574e61-dd63-44c7-a6eb-e8ad63429978",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9fc4850e-1718-4325-86ed-810f61dc6f9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 403,
    "xorig": 0,
    "yorig": 22
}