{
    "id": "76832fec-9227-475c-8db5-e619b0f39532",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "main_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1365,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db140c79-3cf0-4f9a-b636-d38421766758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76832fec-9227-475c-8db5-e619b0f39532",
            "compositeImage": {
                "id": "d62ea413-1f7a-4eef-ab13-d4d635cd8b15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db140c79-3cf0-4f9a-b636-d38421766758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24b42e59-b811-4998-b649-6aafccbb5b3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db140c79-3cf0-4f9a-b636-d38421766758",
                    "LayerId": "4be237fc-b0e0-4ad4-bb7b-972958b10694"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "4be237fc-b0e0-4ad4-bb7b-972958b10694",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76832fec-9227-475c-8db5-e619b0f39532",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1366,
    "xorig": 683,
    "yorig": 384
}