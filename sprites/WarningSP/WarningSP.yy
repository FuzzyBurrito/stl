{
    "id": "84cd8f23-f8e9-49d0-9710-5db4341a3363",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "WarningSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 505,
    "bbox_left": 15,
    "bbox_right": 887,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3bee08ca-db4e-47f0-9f96-a210ae219e7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84cd8f23-f8e9-49d0-9710-5db4341a3363",
            "compositeImage": {
                "id": "78eff5c2-db68-44f1-b557-3cab79eda188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bee08ca-db4e-47f0-9f96-a210ae219e7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a60e6c39-06c6-40ec-bbfc-66aea45a0d0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bee08ca-db4e-47f0-9f96-a210ae219e7a",
                    "LayerId": "30805069-e238-4367-8602-5435988cf7af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 518,
    "layers": [
        {
            "id": "30805069-e238-4367-8602-5435988cf7af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84cd8f23-f8e9-49d0-9710-5db4341a3363",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 450,
    "yorig": 259
}