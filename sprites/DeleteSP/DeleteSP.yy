{
    "id": "cd2db53f-901b-4b4b-8614-89c7f19de3bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "DeleteSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 27,
    "bbox_right": 54,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd46098e-d60b-4f59-979d-f20ba3e11b94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd2db53f-901b-4b4b-8614-89c7f19de3bb",
            "compositeImage": {
                "id": "8280ae66-7757-4965-9cf4-bc5d9100c920",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd46098e-d60b-4f59-979d-f20ba3e11b94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a634501e-5a40-45f8-8edc-a947045a9de9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd46098e-d60b-4f59-979d-f20ba3e11b94",
                    "LayerId": "f9bb93b9-a77b-4dc4-a14b-651559f69969"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "f9bb93b9-a77b-4dc4-a14b-651559f69969",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd2db53f-901b-4b4b-8614-89c7f19de3bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 82,
    "xorig": 41,
    "yorig": 25
}