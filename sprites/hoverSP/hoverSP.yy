{
    "id": "341639b3-2314-4dd3-9562-bf03a6305261",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hoverSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 0,
    "bbox_right": 259,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c49e7fcd-5270-4efb-9d50-3971e2c90155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "341639b3-2314-4dd3-9562-bf03a6305261",
            "compositeImage": {
                "id": "f2ee8550-656b-4cce-85dc-9cc749d0afb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c49e7fcd-5270-4efb-9d50-3971e2c90155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdffc21c-3af4-455c-b6d6-c1658502a2d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c49e7fcd-5270-4efb-9d50-3971e2c90155",
                    "LayerId": "615d636e-5bc8-4419-82a1-db55db2d148b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 91,
    "layers": [
        {
            "id": "615d636e-5bc8-4419-82a1-db55db2d148b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "341639b3-2314-4dd3-9562-bf03a6305261",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 260,
    "xorig": 0,
    "yorig": 45
}