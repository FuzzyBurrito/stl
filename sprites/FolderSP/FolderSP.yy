{
    "id": "7f0b5efd-a6ab-4aad-8ce7-d245f0062c8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "FolderSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 22,
    "bbox_right": 68,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c97f9da-8fe9-412b-9586-a5177084e2db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f0b5efd-a6ab-4aad-8ce7-d245f0062c8e",
            "compositeImage": {
                "id": "063757e7-361e-405f-a210-aeea145f2863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c97f9da-8fe9-412b-9586-a5177084e2db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54be68a2-35d2-489b-8d3d-f168d6387d38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c97f9da-8fe9-412b-9586-a5177084e2db",
                    "LayerId": "eb94dc80-791f-4b7e-84c5-cd3a5611ac13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "eb94dc80-791f-4b7e-84c5-cd3a5611ac13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f0b5efd-a6ab-4aad-8ce7-d245f0062c8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 82,
    "xorig": 41,
    "yorig": 25
}