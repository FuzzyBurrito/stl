{
    "id": "110e8e2e-9078-4601-82f6-3b0eb7febd08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9525ee6c-de68-42d0-8bba-8f17740bae71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "110e8e2e-9078-4601-82f6-3b0eb7febd08",
            "compositeImage": {
                "id": "f06d14d2-d8d9-4072-90d1-6465e15da6a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9525ee6c-de68-42d0-8bba-8f17740bae71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5bdfae5-853e-4eb1-92a6-420ee1b1d693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9525ee6c-de68-42d0-8bba-8f17740bae71",
                    "LayerId": "25ed5a0c-5103-40f0-8fb0-e70ecefbeb6e"
                }
            ]
        },
        {
            "id": "5fae8019-41fc-4183-a7d0-d125d02362d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "110e8e2e-9078-4601-82f6-3b0eb7febd08",
            "compositeImage": {
                "id": "66b508ce-710b-4a37-891d-e259d82e749e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fae8019-41fc-4183-a7d0-d125d02362d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dff0f9fb-27d9-4954-af00-2937fc4959b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fae8019-41fc-4183-a7d0-d125d02362d6",
                    "LayerId": "25ed5a0c-5103-40f0-8fb0-e70ecefbeb6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "25ed5a0c-5103-40f0-8fb0-e70ecefbeb6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "110e8e2e-9078-4601-82f6-3b0eb7febd08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}