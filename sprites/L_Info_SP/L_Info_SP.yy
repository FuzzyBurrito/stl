{
    "id": "98fa9469-58a8-4b1d-ad58-f2fb0d3a7a76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "L_Info_SP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 505,
    "bbox_left": 15,
    "bbox_right": 887,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20944efb-e509-417d-b419-299818f49561",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98fa9469-58a8-4b1d-ad58-f2fb0d3a7a76",
            "compositeImage": {
                "id": "a270865f-c652-47a4-ad20-da723500f545",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20944efb-e509-417d-b419-299818f49561",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7acec6d-60cc-4c5b-94e2-12b7e2729956",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20944efb-e509-417d-b419-299818f49561",
                    "LayerId": "1b3b21b3-93e0-49ee-9720-abe6eac2d60a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 518,
    "layers": [
        {
            "id": "1b3b21b3-93e0-49ee-9720-abe6eac2d60a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98fa9469-58a8-4b1d-ad58-f2fb0d3a7a76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 450,
    "yorig": 259
}