{
    "id": "b3535dd3-e687-4555-a9fe-0cbf08e67bac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "arrows",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9c953f2-0557-464f-9d55-7f2916260739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3535dd3-e687-4555-a9fe-0cbf08e67bac",
            "compositeImage": {
                "id": "35f65a35-50cf-4216-a24f-f550c2086441",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9c953f2-0557-464f-9d55-7f2916260739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2a0d3dc-5e8a-4fcb-ab84-b67280af1f25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9c953f2-0557-464f-9d55-7f2916260739",
                    "LayerId": "674b7077-b5d5-47d7-8d6d-1fd33785c906"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "674b7077-b5d5-47d7-8d6d-1fd33785c906",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3535dd3-e687-4555-a9fe-0cbf08e67bac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}