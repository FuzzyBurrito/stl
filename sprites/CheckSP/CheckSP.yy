{
    "id": "4fed209b-f81b-47bb-8ca4-febfc20e5900",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "CheckSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a896589-abc6-42ec-aa06-ac66a96788cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fed209b-f81b-47bb-8ca4-febfc20e5900",
            "compositeImage": {
                "id": "9840650e-eaec-4d4e-b2ed-649c6870a208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a896589-abc6-42ec-aa06-ac66a96788cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63bbedff-0400-4763-a9b7-101d1e2d768a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a896589-abc6-42ec-aa06-ac66a96788cb",
                    "LayerId": "645870f2-ed5d-40f1-9d09-e34937aa64ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "645870f2-ed5d-40f1-9d09-e34937aa64ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4fed209b-f81b-47bb-8ca4-febfc20e5900",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 19,
    "yorig": 18
}