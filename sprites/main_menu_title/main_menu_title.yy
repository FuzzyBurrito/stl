{
    "id": "3709830a-2921-4a23-8d04-7a5863a86ecc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "main_menu_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 171,
    "bbox_left": 0,
    "bbox_right": 798,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b93f648-953f-4d06-9e49-2863bc713351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3709830a-2921-4a23-8d04-7a5863a86ecc",
            "compositeImage": {
                "id": "44443e94-005b-4e7f-8660-3e0bffdc03d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b93f648-953f-4d06-9e49-2863bc713351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d01f706-a42e-4fbd-9f71-1efa7d312807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b93f648-953f-4d06-9e49-2863bc713351",
                    "LayerId": "8828431c-e65c-4443-97d1-46abcbefcc6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 182,
    "layers": [
        {
            "id": "8828431c-e65c-4443-97d1-46abcbefcc6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3709830a-2921-4a23-8d04-7a5863a86ecc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 400,
    "yorig": 91
}