{
    "id": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "boxSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 36,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "274f3fff-0a16-4976-8e21-6cffeaacc5e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
            "compositeImage": {
                "id": "247c51f1-8922-4e04-bd0f-ecfefffb249c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "274f3fff-0a16-4976-8e21-6cffeaacc5e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb133ee1-dd12-4715-933d-889d686ce59b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "274f3fff-0a16-4976-8e21-6cffeaacc5e7",
                    "LayerId": "5bf48cd2-a9da-4650-b1db-d5dcd996bb09"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "5bf48cd2-a9da-4650-b1db-d5dcd996bb09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 19,
    "yorig": 18
}