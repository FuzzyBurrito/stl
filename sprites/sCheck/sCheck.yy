{
    "id": "fe56f527-12c5-46f8-9075-1620c49862aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCheck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5867c891-3050-49c1-8ac0-da325a74c7ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe56f527-12c5-46f8-9075-1620c49862aa",
            "compositeImage": {
                "id": "3f829cb7-9c05-4ef1-bfd1-e0937e8df4f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5867c891-3050-49c1-8ac0-da325a74c7ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b9bb1fd-64dc-4f70-bdf9-8a1f2aa970c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5867c891-3050-49c1-8ac0-da325a74c7ee",
                    "LayerId": "853af046-7270-49a1-8cf2-692e4f6908eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "853af046-7270-49a1-8cf2-692e4f6908eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe56f527-12c5-46f8-9075-1620c49862aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}