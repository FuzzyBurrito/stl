{
    "id": "fddb9883-ca3d-4ef3-a6a2-c40c9a616923",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ZipSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 23,
    "bbox_right": 57,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5023530d-43de-497b-a8be-d3e93112fdb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddb9883-ca3d-4ef3-a6a2-c40c9a616923",
            "compositeImage": {
                "id": "dda6f6ae-b1f5-409c-8f23-11e39863075f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5023530d-43de-497b-a8be-d3e93112fdb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad9df9fb-e734-4f4e-a66c-6989dc4d3aea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5023530d-43de-497b-a8be-d3e93112fdb0",
                    "LayerId": "a6d6b742-d068-4384-bd16-55de1e639a1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "a6d6b742-d068-4384-bd16-55de1e639a1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fddb9883-ca3d-4ef3-a6a2-c40c9a616923",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 82,
    "xorig": 41,
    "yorig": 25
}