{
    "id": "06687aff-aac5-484b-9294-0ce037d3c9db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ButtonLayoutSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 271,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b481a502-8f95-4c08-ba43-99c04524cf78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06687aff-aac5-484b-9294-0ce037d3c9db",
            "compositeImage": {
                "id": "9e013699-0644-4505-8c23-783f7798caf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b481a502-8f95-4c08-ba43-99c04524cf78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e20a14e-fe25-4c17-9327-62e5d4ab8c91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b481a502-8f95-4c08-ba43-99c04524cf78",
                    "LayerId": "e3001c9b-bb8b-4cb2-989f-e718a7e9e8c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "e3001c9b-bb8b-4cb2-989f-e718a7e9e8c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06687aff-aac5-484b-9294-0ce037d3c9db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 272,
    "xorig": 136,
    "yorig": 51
}