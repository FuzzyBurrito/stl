{
    "id": "26466b1f-c406-469d-9177-6935c1e01567",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "iSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 21,
    "bbox_right": 61,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69803812-21e9-4f15-ab4e-1ccc2d0b69ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26466b1f-c406-469d-9177-6935c1e01567",
            "compositeImage": {
                "id": "b78ebee1-ba09-44b6-882c-5176b29da614",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69803812-21e9-4f15-ab4e-1ccc2d0b69ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da749300-bff2-4007-b53f-0c0b46e9a599",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69803812-21e9-4f15-ab4e-1ccc2d0b69ce",
                    "LayerId": "da3865c1-5824-445f-a62d-b6367fd8158f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "da3865c1-5824-445f-a62d-b6367fd8158f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26466b1f-c406-469d-9177-6935c1e01567",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 82,
    "xorig": 41,
    "yorig": 25
}