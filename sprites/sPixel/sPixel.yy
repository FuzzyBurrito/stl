{
    "id": "76e199ec-cd06-494b-9a7d-cc76d945cafd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPixel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aac8fad4-e397-41d2-a992-671326807a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76e199ec-cd06-494b-9a7d-cc76d945cafd",
            "compositeImage": {
                "id": "42784390-8c81-4280-945a-e01cc5b6946b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aac8fad4-e397-41d2-a992-671326807a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8deea9f-77d4-404f-a087-5c940f52eaf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aac8fad4-e397-41d2-a992-671326807a0c",
                    "LayerId": "d224ea6b-a83f-4100-88c0-a49a623c4876"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "d224ea6b-a83f-4100-88c0-a49a623c4876",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76e199ec-cd06-494b-9a7d-cc76d945cafd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}