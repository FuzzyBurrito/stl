{
    "id": "be52f8ea-887b-4bd8-b25b-a1304f8c04c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "frameSP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 271,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c282eb2-e2c6-48d7-a3bf-450f5797a566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be52f8ea-887b-4bd8-b25b-a1304f8c04c7",
            "compositeImage": {
                "id": "caffa375-c30e-4022-a045-04fbb1b76889",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c282eb2-e2c6-48d7-a3bf-450f5797a566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3cb4d28-364e-496f-93ec-0d236870595c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c282eb2-e2c6-48d7-a3bf-450f5797a566",
                    "LayerId": "7da21c86-c769-4d3a-bc7e-3b65188fe89a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "7da21c86-c769-4d3a-bc7e-3b65188fe89a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be52f8ea-887b-4bd8-b25b-a1304f8c04c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 272,
    "xorig": 136,
    "yorig": 51
}