{
    "id": "582770a9-b5a8-4f5c-8903-cc78b08d3bbe",
    "modelName": "GMExtension",
    "mvc": "1.2",
    "name": "FileDropper",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 202375362,
    "date": "2019-22-07 04:05:47",
    "description": "",
    "exportToGame": true,
    "extensionName": "",
    "files": [
        {
            "id": "b1aaebd4-5432-4dbe-8d1a-297f70abc76d",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 1048640,
            "filename": "FileDropper.dll",
            "final": "",
            "functions": [
                {
                    "id": "b5866ff5-c92f-4da9-8ee1-301e2e3be893",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "file_dnd_get_enabled",
                    "help": "file_dnd_get_enabled()",
                    "hidden": false,
                    "kind": 12,
                    "name": "file_dnd_get_enabled",
                    "returnType": 2
                },
                {
                    "id": "4890f0fa-2339-47b2-b68f-08fcc03d9d21",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "file_dnd_set_enabled",
                    "help": "file_dnd_set_enabled(enable)",
                    "hidden": false,
                    "kind": 12,
                    "name": "file_dnd_set_enabled",
                    "returnType": 2
                },
                {
                    "id": "4907501f-de3d-4dce-8d0b-2bb1ab847103",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "file_dnd_get_files",
                    "help": "file_dnd_get_files()",
                    "hidden": false,
                    "kind": 12,
                    "name": "file_dnd_get_files",
                    "returnType": 1
                },
                {
                    "id": "0ab28f2f-b3d9-4bcf-995e-766adb224e5c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        1,
                        2,
                        2,
                        2
                    ],
                    "externalName": "file_dnd_set_files",
                    "help": "file_dnd_set_files(pattern,allowfiles,allowdirs,allowmulti)",
                    "hidden": false,
                    "kind": 12,
                    "name": "file_dnd_set_files",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 1,
            "order": [
                
            ],
            "origname": "extensions\\FileDropper.dll",
            "uncompress": false
        },
        {
            "id": "c458cbdd-1550-47fa-b2a2-439cbdb91180",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 0,
            "filename": "FileDropper.dll.zip",
            "final": "",
            "functions": [
                
            ],
            "init": "",
            "kind": 4,
            "order": [
                
            ],
            "origname": "extensions\\FileDropper.dll.zip",
            "uncompress": false
        },
        {
            "id": "aa5680f4-4d4e-44eb-888c-25c28d59e05a",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 67108866,
            "filename": "FileDropper.dylib",
            "final": "",
            "functions": [
                {
                    "id": "f569ec09-8312-46ca-b5c1-e571431a08b3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "file_dnd_get_enabled",
                    "help": "file_dnd_get_enabled()",
                    "hidden": false,
                    "kind": 12,
                    "name": "file_dnd_get_enabled",
                    "returnType": 2
                },
                {
                    "id": "19dea6bd-cc56-4ca2-becd-94ffafea14b7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        2
                    ],
                    "externalName": "file_dnd_set_enabled",
                    "help": "file_dnd_set_enabled(enable)",
                    "hidden": false,
                    "kind": 12,
                    "name": "file_dnd_set_enabled",
                    "returnType": 2
                },
                {
                    "id": "51b24834-7c33-460b-a234-7db7b9d30b2f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "file_dnd_get_files",
                    "help": "file_dnd_get_files()",
                    "hidden": false,
                    "kind": 12,
                    "name": "file_dnd_get_files",
                    "returnType": 1
                },
                {
                    "id": "d5de68bc-2cf0-4ba2-8365-010c58b6f06e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        1,
                        2,
                        2,
                        2
                    ],
                    "externalName": "file_dnd_set_files",
                    "help": "file_dnd_set_files(pattern,allowfiles,allowdirs,allowmulti)",
                    "hidden": false,
                    "kind": 12,
                    "name": "file_dnd_set_files",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 1,
            "order": [
                
            ],
            "origname": "extensions\\FileDropper.dylib",
            "uncompress": false
        },
        {
            "id": "d94133d5-f406-4009-bb53-114383fd496c",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 0,
            "filename": "FileDropper.dylib.zip",
            "final": "",
            "functions": [
                
            ],
            "init": "",
            "kind": 4,
            "order": [
                
            ],
            "origname": "extensions\\FileDropper.dylib.zip",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "options": null,
    "optionsFile": "options.json",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "supportedTargets": 202375362,
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "1.0.0"
}