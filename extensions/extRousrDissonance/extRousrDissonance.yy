{
    "id": "31dd75ce-dbbe-4e77-a35b-baa910f9a057",
    "modelName": "GMExtension",
    "mvc": "1.2",
    "name": "extRousrDissonance",
    "IncludedResources": [
        "Sprites\\bg",
        "Sprites\\sCheck",
        "Sprites\\sButton",
        "Sprites\\sPixel",
        "Scripts\\Dissonance\\rousr_dissonance_create",
        "Scripts\\Dissonance\\rousr_dissonance_dummy_function",
        "Scripts\\Dissonance\\rousr_dissonance_handler_on_ready",
        "Scripts\\Dissonance\\rousr_dissonance_handler_on_disconnected",
        "Scripts\\Dissonance\\rousr_dissonance_handler_on_error",
        "Scripts\\Dissonance\\rousr_dissonance_handler_on_join",
        "Scripts\\Dissonance\\rousr_dissonance_handler_on_spectate",
        "Scripts\\Dissonance\\rousr_dissonance_handler_on_join_request",
        "Scripts\\Dissonance\\rousr_dissonance_respond_to_join",
        "Scripts\\Dissonance\\rousr_dissonance_set_details",
        "Scripts\\Dissonance\\rousr_dissonance_set_large_image",
        "Scripts\\Dissonance\\rousr_dissonance_set_small_image",
        "Scripts\\Dissonance\\rousr_dissonance_set_timestamps",
        "Scripts\\Dissonance\\rousr_dissonance_set_state",
        "Scripts\\Dissonance\\rousr_dissonance_set_party",
        "Scripts\\Dissonance\\rousr_dissonance_set_join_secret",
        "Scripts\\Dissonance\\rousr_dissonance_set_spectate_secret",
        "Scripts\\Dissonance\\rousr_dissonance_set_match_secret",
        "Scripts\\Dissonance\\internal\\rousrDissonance_event_create",
        "Scripts\\Dissonance\\internal\\rousrDissonance_event_step",
        "Scripts\\Dissonance\\internal\\rousrDissonance_event_end_step",
        "Scripts\\Dissonance\\internal\\rousrDissonance_event_async_social",
        "Scripts\\Dissonance\\internal\\gmlscripts_unix_timestamp",
        "Scripts\\Example\\button_can_click",
        "Scripts\\Example\\example_click_accept",
        "Scripts\\Example\\example_click_ignore",
        "Scripts\\Example\\example_click_reject",
        "Scripts\\Example\\example_on_disconnected",
        "Scripts\\Example\\example_on_error",
        "Scripts\\Example\\example_on_join",
        "Scripts\\Example\\example_on_join_request",
        "Scripts\\Example\\example_on_ready",
        "Scripts\\Example\\example_on_spectate",
        "Scripts\\Example\\example_text_field",
        "Scripts\\Example\\example_toggle",
        "Scripts\\Example\\random_key",
        "Scripts\\ReadMe",
        "Fonts\\fnt_game",
        "Objects\\Dissonance\\rousrDissonance",
        "Objects\\Example\\Example",
        "Objects\\Example\\Button",
        "Objects\\Example\\CheckBox",
        "Objects\\Example\\TextField",
        "Objects\\Example\\logo",
        "Rooms\\rm_demo",
        "Included Files\\Example_Assets__App_Dashboard_\\dissonance_large.png",
        "Included Files\\Example_Assets__App_Dashboard_\\dissonance_small.png",
        "Included Files\\04b24.TTF",
        "Included Files\\README.md"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 105553859707074,
    "date": "2019-37-09 09:08:52",
    "description": "",
    "exportToGame": true,
    "extensionName": "",
    "files": [
        {
            "id": "59df5dc1-40e5-4f99-b8d0-edb7c875c7da",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 105553859707074,
            "filename": "rousrDissonance.dll",
            "final": "discord_shutdown",
            "functions": [
                {
                    "id": "dc90e546-a9c7-4004-8b49-6d3237066f54",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "Init",
                    "help": "initialize Discord with the application ID and Steam ID (_application_id, [_steam_id])",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_init",
                    "returnType": 2
                },
                {
                    "id": "873975b4-7897-4c41-966e-5424cb3a1ecc",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "UpdatePresence",
                    "help": "send the presences updates to Discord, call after sets.",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_update_presence",
                    "returnType": 2
                },
                {
                    "id": "14328b2c-b791-4cb2-8906-18157891a679",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "RunCallbacks",
                    "help": "run any pending discord call backs - call once per step",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_run_callbacks",
                    "returnType": 2
                },
                {
                    "id": "81e473d4-4d6a-4c3b-ba60-04f9b5c9f154",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ResetPresence",
                    "help": "clears all presence data currently set this session",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_reset_presence",
                    "returnType": 2
                },
                {
                    "id": "9ff993ba-63a8-4690-ac70-bfd69a76f8f2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "SetState",
                    "help": "set the state string (_state)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_state",
                    "returnType": 2
                },
                {
                    "id": "6b020af0-f6ed-4947-b4aa-826b3ee3b053",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "SetDetails",
                    "help": "set the details string (_details)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_details",
                    "returnType": 2
                },
                {
                    "id": "602f88ab-f923-423c-ad0d-1b4bf2e7bb23",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        2,
                        2,
                        2,
                        2
                    ],
                    "externalName": "SetTimeStamps",
                    "help": "set the start and end timestamp, in unix time format (_start_lo, _start_hi, _end_lo, _end_hi)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_timestamps",
                    "returnType": 2
                },
                {
                    "id": "508a69e9-d06d-4fc6-921f-77a1d401c263",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "SetLargeImage",
                    "help": "set the large image detail and caption  (_large_image_key, _large_image_text)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_large_image",
                    "returnType": 2
                },
                {
                    "id": "d7a6d171-b2fe-462d-a14a-bc3580ce8305",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "SetSmallImage",
                    "help": "set the small image detail and caption (_small_image_key, _small_image_text)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_small_image",
                    "returnType": 2
                },
                {
                    "id": "8e20b2de-7e75-4107-9aa1-c55b86eb39dc",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        1,
                        2,
                        2
                    ],
                    "externalName": "SetPartyData",
                    "help": "set the party id and member count data (_party_id, _party_count, _party_max)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_party",
                    "returnType": 2
                },
                {
                    "id": "58b7f0ce-0bae-4e43-a218-8139f3fa077f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "SetMatchSecret",
                    "help": "set the match secret, and whether or not the match represents a finite \"match\" (_match_secret, _instance)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_match_secret",
                    "returnType": 2
                },
                {
                    "id": "c11dded8-9e80-452e-9319-c862f5b4fc5f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "SetJoinSecret",
                    "help": "set the unique, encrypted join secret token (_join_secret)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_join_secret",
                    "returnType": 2
                },
                {
                    "id": "274cdf72-b4fa-4cb0-bd26-7de2482cb250",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "SetSpectateSecret",
                    "help": "set the unique, encrypted spectate secret token (_spectate_secret)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_spectate_secret",
                    "returnType": 2
                },
                {
                    "id": "2ac203ce-2ece-4600-bfb2-6d433aec93a8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "Respond",
                    "help": "send a reply to `ask to join` request (_user_id, _reply)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_respond",
                    "returnType": 2
                },
                {
                    "id": "8dccd083-135f-4f8f-8f0e-f6aa9201b28c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        1,
                        1,
                        1,
                        1
                    ],
                    "externalName": "RegisterCallbacks",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "RegisterCallbacks",
                    "returnType": 2
                },
                {
                    "id": "460168bf-f746-4236-bc4d-89c3f551f96c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "Shutdown",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_shutdown",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 1,
            "order": [
                
            ],
            "origname": "extensions\\rousrDissonance.dll",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": null,
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "options": null,
    "optionsFile": "options.json",
    "packageID": "com.rousr.dissonance",
    "productID": "1E83191CE77E300EE0DC1270C217654D",
    "sourcedir": "",
    "supportedTargets": 105553859707074,
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": null,
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "1.0.3"
}