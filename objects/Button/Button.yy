{
    "id": "e6a5eabd-f6a5-4924-8c89-5c6776f16dbb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Button",
    "eventList": [
        {
            "id": "df2e02db-a201-47e0-8c2f-86db80658d22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e6a5eabd-f6a5-4924-8c89-5c6776f16dbb"
        },
        {
            "id": "ec54ba2d-830b-4755-a388-0d47447cbb90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "e6a5eabd-f6a5-4924-8c89-5c6776f16dbb"
        },
        {
            "id": "60c951d5-0fd0-45d3-8a7e-9ec50843209e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "e6a5eabd-f6a5-4924-8c89-5c6776f16dbb"
        },
        {
            "id": "e8f2138f-791e-46cf-9992-8285c107e5d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "e6a5eabd-f6a5-4924-8c89-5c6776f16dbb"
        },
        {
            "id": "13879eea-0452-440c-b9bb-1057d5ab08c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "e6a5eabd-f6a5-4924-8c89-5c6776f16dbb"
        },
        {
            "id": "4bdbc37d-456b-404f-90ec-710c69469587",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "e6a5eabd-f6a5-4924-8c89-5c6776f16dbb"
        },
        {
            "id": "ca48cb41-5c63-4034-9062-6a0d62515adf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e6a5eabd-f6a5-4924-8c89-5c6776f16dbb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "110e8e2e-9078-4601-82f6-3b0eb7febd08",
    "visible": true
}