{
    "id": "7114bb3c-6749-4044-8f36-44ce905e7228",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Toggle_3_1",
    "eventList": [
        {
            "id": "72aeb91b-be48-4583-b9cb-73b8ab2a078f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7114bb3c-6749-4044-8f36-44ce905e7228"
        },
        {
            "id": "a2f2b620-47e3-4d1e-b2ea-aa7a7ccb7780",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7114bb3c-6749-4044-8f36-44ce905e7228"
        },
        {
            "id": "23ff028c-7202-4b12-a37c-8c10d5d50d32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "7114bb3c-6749-4044-8f36-44ce905e7228"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}