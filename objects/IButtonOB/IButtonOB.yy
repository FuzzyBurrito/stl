{
    "id": "75fcfb87-c40d-411c-900a-5b8ca1e7d1fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "IButtonOB",
    "eventList": [
        {
            "id": "f959acd7-4738-4647-9111-3df969fe17e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "75fcfb87-c40d-411c-900a-5b8ca1e7d1fa"
        },
        {
            "id": "b7d97178-7792-4f6a-ae6c-3fe6983a5671",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "75fcfb87-c40d-411c-900a-5b8ca1e7d1fa"
        },
        {
            "id": "c973334c-54c3-4244-bdd9-78bec747b744",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "75fcfb87-c40d-411c-900a-5b8ca1e7d1fa"
        },
        {
            "id": "23f7a898-1e61-46cb-9223-338a4cb5519c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "75fcfb87-c40d-411c-900a-5b8ca1e7d1fa"
        },
        {
            "id": "53048006-5fd8-46cc-aa19-509f9048a85c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "75fcfb87-c40d-411c-900a-5b8ca1e7d1fa"
        },
        {
            "id": "8938eca6-400e-4398-88d9-9bc14e8723af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "75fcfb87-c40d-411c-900a-5b8ca1e7d1fa"
        },
        {
            "id": "54718314-9036-4058-9965-bde51b6e45d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 79,
            "eventtype": 9,
            "m_owner": "75fcfb87-c40d-411c-900a-5b8ca1e7d1fa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7321efe8-fdb5-405e-af0b-7336d88fbbac",
    "visible": true
}