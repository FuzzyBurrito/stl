{
    "id": "e73380fd-f2dc-47d1-95e9-e7cf15415608",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "LoadOB",
    "eventList": [
        {
            "id": "250d5fd1-a883-4c70-80bf-f709a8321490",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e73380fd-f2dc-47d1-95e9-e7cf15415608"
        },
        {
            "id": "a139cf41-273a-4115-8e54-a4c962c56f21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e73380fd-f2dc-47d1-95e9-e7cf15415608"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b3535dd3-e687-4555-a9fe-0cbf08e67bac",
    "visible": true
}