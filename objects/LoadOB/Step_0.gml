/// @DnDAction : YoYo Games.Instances.Sprite_Rotate
/// @DnDVersion : 1
/// @DnDHash : 12046E60
/// @DnDArgument : "angle" "rot"
/// @DnDArgument : "angle_relative" "1"
image_angle += rot;

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 431574E0
/// @DnDArgument : "expr" ".002"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "rot"
rot += .002;