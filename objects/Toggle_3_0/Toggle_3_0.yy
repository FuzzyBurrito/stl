{
    "id": "787512cd-a5e7-4429-98c2-d082fc1a3598",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Toggle_3_0",
    "eventList": [
        {
            "id": "2c4660a9-0af0-4b2a-9aec-2812da055054",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "787512cd-a5e7-4429-98c2-d082fc1a3598"
        },
        {
            "id": "a89fdd2a-4ebe-4690-8809-4bbfb7a9de0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "787512cd-a5e7-4429-98c2-d082fc1a3598"
        },
        {
            "id": "35d126fc-d734-408d-942c-8a429fd28a51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "787512cd-a5e7-4429-98c2-d082fc1a3598"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}