{
    "id": "dd89d6f6-2509-46e3-80f6-0c1fbae009c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "DeleteButtonOB",
    "eventList": [
        {
            "id": "a4b6df2f-d73c-4999-aeea-454f165f2b6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dd89d6f6-2509-46e3-80f6-0c1fbae009c4"
        },
        {
            "id": "27ae7e2e-f349-4b9e-9818-c175ebcf7a48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd89d6f6-2509-46e3-80f6-0c1fbae009c4"
        },
        {
            "id": "fcf36a30-0185-4b5d-9788-e6ccb1886603",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "dd89d6f6-2509-46e3-80f6-0c1fbae009c4"
        },
        {
            "id": "20b69ed0-def7-486c-a598-2bdc9556f1b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dd89d6f6-2509-46e3-80f6-0c1fbae009c4"
        },
        {
            "id": "6e641e9d-3e65-49e5-8e5c-002988d1727c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "dd89d6f6-2509-46e3-80f6-0c1fbae009c4"
        },
        {
            "id": "c42f053f-3abc-4f32-ac6a-71b589d31b53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "dd89d6f6-2509-46e3-80f6-0c1fbae009c4"
        },
        {
            "id": "1a07cc03-25c9-4d75-b7b2-87c33c5c1e5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 79,
            "eventtype": 9,
            "m_owner": "dd89d6f6-2509-46e3-80f6-0c1fbae009c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7321efe8-fdb5-405e-af0b-7336d88fbbac",
    "visible": true
}