/// @DnDAction : YoYo Games.Files.If_File_Exists
/// @DnDVersion : 1
/// @DnDHash : 1661C2BC
/// @DnDArgument : "file" ""back.png""
var l1661C2BC_0 = file_exists("back.png");
if(l1661C2BC_0)
{
	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 5D9F2953
	/// @DnDParent : 1661C2BC
	/// @DnDArgument : "code" "spr = sprite_add("back.png",1, true, true,683, 384);$(13_10)"
	spr = sprite_add("back.png",1, true, true,683, 384);

	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 19C33761
	/// @DnDParent : 1661C2BC
	/// @DnDArgument : "spriteind" "spr"
	sprite_index = spr;
	image_index = 0;

	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 63F3EA45
	/// @DnDParent : 1661C2BC
	/// @DnDArgument : "code" "image_xscale = 1366 / sprite_get_width(sprite_index);$(13_10)image_yscale = 768 / sprite_get_height(sprite_index);"
	image_xscale = 1366 / sprite_get_width(sprite_index);
	image_yscale = 768 / sprite_get_height(sprite_index);
}