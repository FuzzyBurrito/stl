/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 30BE35F4
/// @DnDArgument : "code" "draw_self()"
draw_self()

/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
/// @DnDVersion : 1
/// @DnDHash : 71435D0B
/// @DnDArgument : "x" "x"
/// @DnDArgument : "y" "y-60"
/// @DnDArgument : "xscale" "1.1"
/// @DnDArgument : "yscale" "1.1"
/// @DnDArgument : "caption" ""Background""
draw_text_transformed(x, y-60, string("Background") + "", 1.1, 1.1, 0);

/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
/// @DnDVersion : 1
/// @DnDHash : 6D18C506
/// @DnDArgument : "x" "x"
/// @DnDArgument : "y" "y"
/// @DnDArgument : "caption" ""Import""
draw_text_transformed(x, y, string("Import") + "", 1, 1, 0);