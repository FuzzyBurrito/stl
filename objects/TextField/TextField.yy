{
    "id": "8282a306-d594-44ed-8c41-e2454696e258",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TextField",
    "eventList": [
        {
            "id": "23266457-f175-4144-a8a9-1f82a10057f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8282a306-d594-44ed-8c41-e2454696e258"
        },
        {
            "id": "31f30329-05e6-4855-9476-f2f843c3b820",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8282a306-d594-44ed-8c41-e2454696e258"
        },
        {
            "id": "6d54d078-e173-483a-9d8e-1a1ef55ee573",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8282a306-d594-44ed-8c41-e2454696e258"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "76e199ec-cd06-494b-9a7d-cc76d945cafd",
    "visible": true
}