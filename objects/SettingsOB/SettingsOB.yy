{
    "id": "1d0a1b31-e436-40e6-96e1-d5f015b1d507",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "SettingsOB",
    "eventList": [
        {
            "id": "9cad9fa5-0bc2-46fe-9a64-76996a7fa53e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1d0a1b31-e436-40e6-96e1-d5f015b1d507"
        },
        {
            "id": "211e85e9-e727-482a-9410-6d3035f15d77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "1d0a1b31-e436-40e6-96e1-d5f015b1d507"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d9f40d26-910a-4eba-aa03-abc267fc2faa",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9fc4850e-1718-4325-86ed-810f61dc6f9b",
    "visible": true
}