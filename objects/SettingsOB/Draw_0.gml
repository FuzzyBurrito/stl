/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 30BE35F4
/// @DnDArgument : "code" "draw_self()"
draw_self()

/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
/// @DnDVersion : 1
/// @DnDHash : 71435D0B
/// @DnDArgument : "x" "x"
/// @DnDArgument : "y" "y"
/// @DnDArgument : "caption" ""Settings""
draw_text_transformed(x, y, string("Settings") + "", 1, 1, 0);

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 3D8B8006
/// @DnDArgument : "var" "stl_debug"
/// @DnDArgument : "value" "true"
if(stl_debug == true)
{
	/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
	/// @DnDVersion : 1
	/// @DnDHash : 6F7CFF94
	/// @DnDParent : 3D8B8006
	/// @DnDArgument : "x" "100"
	/// @DnDArgument : "y" "352"
	/// @DnDArgument : "xscale" ".5"
	/// @DnDArgument : "yscale" ".5"
	/// @DnDArgument : "caption" "global.game_dir"
	/// @DnDArgument : "text" "global.exe_name"
	draw_text_transformed(100, 352, string(global.game_dir) + string(global.exe_name), .5, .5, 0);
}