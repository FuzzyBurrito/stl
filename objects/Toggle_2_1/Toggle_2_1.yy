{
    "id": "ea67a451-b51d-4b0e-ac1a-bdb09a0450d0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Toggle_2_1",
    "eventList": [
        {
            "id": "a1f0fb07-4c55-497f-8ab0-31dcaa83cbd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ea67a451-b51d-4b0e-ac1a-bdb09a0450d0"
        },
        {
            "id": "991eba88-146d-4596-8564-c0900afc4543",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ea67a451-b51d-4b0e-ac1a-bdb09a0450d0"
        },
        {
            "id": "cf974ed1-8d4d-4075-ae28-7726e5c0501c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "ea67a451-b51d-4b0e-ac1a-bdb09a0450d0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}