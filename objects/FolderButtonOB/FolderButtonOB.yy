{
    "id": "b903c86e-7982-4ce5-a5a9-b67cf2c72435",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "FolderButtonOB",
    "eventList": [
        {
            "id": "539ed365-1913-43c3-896a-0eddfd990fce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b903c86e-7982-4ce5-a5a9-b67cf2c72435"
        },
        {
            "id": "5951f0f7-34e1-4215-9c2a-c874cf41376e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b903c86e-7982-4ce5-a5a9-b67cf2c72435"
        },
        {
            "id": "706c7552-ccc2-44c4-9926-074b17fbea99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b903c86e-7982-4ce5-a5a9-b67cf2c72435"
        },
        {
            "id": "14c3dfc9-1ec6-4604-b901-c0abe2c42719",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b903c86e-7982-4ce5-a5a9-b67cf2c72435"
        },
        {
            "id": "6eb85026-6e9c-4aac-87f1-28f31c061cf1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "b903c86e-7982-4ce5-a5a9-b67cf2c72435"
        },
        {
            "id": "9732aa77-b657-46c5-82f6-d594b5662180",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "b903c86e-7982-4ce5-a5a9-b67cf2c72435"
        },
        {
            "id": "1c4a341c-d8b3-4d9e-ac29-c45d25dad929",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 79,
            "eventtype": 9,
            "m_owner": "b903c86e-7982-4ce5-a5a9-b67cf2c72435"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7321efe8-fdb5-405e-af0b-7336d88fbbac",
    "visible": true
}