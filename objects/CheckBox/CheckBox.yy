{
    "id": "eda9ab02-5fe6-41e0-8e84-acf8db4cbb49",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "CheckBox",
    "eventList": [
        {
            "id": "a881750b-7ad8-4b46-be4e-1715425c8583",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eda9ab02-5fe6-41e0-8e84-acf8db4cbb49"
        },
        {
            "id": "9396ca6f-2df6-4bdb-975f-2cd35821ca1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "eda9ab02-5fe6-41e0-8e84-acf8db4cbb49"
        },
        {
            "id": "9a77f113-f7e5-4331-ba42-05e8631d2009",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "eda9ab02-5fe6-41e0-8e84-acf8db4cbb49"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "76e199ec-cd06-494b-9a7d-cc76d945cafd",
    "visible": true
}