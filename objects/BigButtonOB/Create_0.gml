if (os_type == os_windows)
{
	if(!(global.selected_version == "RAGS-2.0-all"))
	{
		global.exe_name = "/StudentTransfer.exe"
	}
	else
	{
		global.exe_name = "/StudentTransfer-RAGS.exe"
	}
}

if (os_type == os_linux)
{
	if(!(global.selected_version == "RAGS-2.0-all"))
	{
		global.exe_name = "/StudentTransfer.sh"
	}
	else
	{
		global.exe_name = "/StudentTransfer-RAGS.sh"
	}
}

var l4FD8DEA8_0 = file_exists(global.game_dir+global.exe_name);
if(l4FD8DEA8_0)
{

	image_blend = $FFAD07B2 & $ffffff;
	image_alpha = ($FFAD07B2 >> 24) / $ff;
}

else
{
	image_blend = $FF343434 & $ffffff;
	image_alpha = ($FF343434 >> 24) / $ff;
}
