{
    "id": "787a358d-5db4-40ff-b28b-8f6892096ef0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "BigButtonOB",
    "eventList": [
        {
            "id": "400ca2e7-6d1a-4c7f-974c-2cf8b6a28a5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "787a358d-5db4-40ff-b28b-8f6892096ef0"
        },
        {
            "id": "b2824238-8bac-4305-9cf5-ab3dcce4d208",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "787a358d-5db4-40ff-b28b-8f6892096ef0"
        },
        {
            "id": "1471602b-c83d-4859-b0ea-3ee37023d00f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "787a358d-5db4-40ff-b28b-8f6892096ef0"
        },
        {
            "id": "e9786c1a-c4ca-4a45-97e0-170ed6894883",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "787a358d-5db4-40ff-b28b-8f6892096ef0"
        },
        {
            "id": "046cbbdd-d6e1-4eea-9484-37b638bed96b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "787a358d-5db4-40ff-b28b-8f6892096ef0"
        },
        {
            "id": "e8529091-8288-406a-b771-09519cf85d5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "787a358d-5db4-40ff-b28b-8f6892096ef0"
        },
        {
            "id": "7731a569-40c2-4a16-8ae5-77ca58768da9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 79,
            "eventtype": 9,
            "m_owner": "787a358d-5db4-40ff-b28b-8f6892096ef0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "06687aff-aac5-484b-9294-0ce037d3c9db",
    "visible": true
}