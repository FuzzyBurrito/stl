{
    "id": "d744e74b-1fbe-4a30-833a-99265a4fdfb9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "resetBackOB",
    "eventList": [
        {
            "id": "87ac7576-dd0c-496e-bcd0-d7934e6b38d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d744e74b-1fbe-4a30-833a-99265a4fdfb9"
        },
        {
            "id": "8ab15723-8e36-440f-9764-1a58bb6bcfec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d744e74b-1fbe-4a30-833a-99265a4fdfb9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d9f40d26-910a-4eba-aa03-abc267fc2faa",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9fc4850e-1718-4325-86ed-810f61dc6f9b",
    "visible": true
}