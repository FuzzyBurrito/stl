{
    "id": "b5a993d2-51c8-4a1f-bb80-cee1f088bbed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Example",
    "eventList": [
        {
            "id": "aaa55807-50b3-4bf8-9048-4b1a79c4371e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b5a993d2-51c8-4a1f-bb80-cee1f088bbed"
        },
        {
            "id": "9bd503e0-e952-498a-b99a-cc74650f6053",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b5a993d2-51c8-4a1f-bb80-cee1f088bbed"
        },
        {
            "id": "28cf43b2-b3ff-431b-bdad-0110a270448d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b5a993d2-51c8-4a1f-bb80-cee1f088bbed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}