{
    "id": "f6cde71f-0ade-483a-b403-8eaf04308a7d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Toggle_1_0",
    "eventList": [
        {
            "id": "c1178544-6ebb-448f-a8b5-c93f2795b549",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f6cde71f-0ade-483a-b403-8eaf04308a7d"
        },
        {
            "id": "f18eed16-958b-4b42-9bd3-1005a5d42484",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f6cde71f-0ade-483a-b403-8eaf04308a7d"
        },
        {
            "id": "e4e39d32-0dc6-4e5c-b4bc-cbbac379b78d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "f6cde71f-0ade-483a-b403-8eaf04308a7d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}