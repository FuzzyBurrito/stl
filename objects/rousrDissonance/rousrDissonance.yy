{
    "id": "6d9dd413-df41-40b3-9a3b-7eef9217ef1a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "rousrDissonance",
    "eventList": [
        {
            "id": "e36405bd-0375-421c-824b-faba9c8cc24e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6d9dd413-df41-40b3-9a3b-7eef9217ef1a"
        },
        {
            "id": "a0d5fc30-3a21-487a-baa7-36bc029410e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "6d9dd413-df41-40b3-9a3b-7eef9217ef1a"
        },
        {
            "id": "9b825299-25b9-4c32-bb98-f813ebb8baca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6d9dd413-df41-40b3-9a3b-7eef9217ef1a"
        },
        {
            "id": "8118ebe6-2273-4581-9517-a0c54c5e94ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 70,
            "eventtype": 7,
            "m_owner": "6d9dd413-df41-40b3-9a3b-7eef9217ef1a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}