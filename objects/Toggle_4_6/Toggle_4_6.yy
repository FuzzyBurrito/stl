{
    "id": "ede16bcd-2819-4bc6-bbdc-20d9655e5e0b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Toggle_4_6",
    "eventList": [
        {
            "id": "0eff7d36-6e33-43ae-93b2-c32c03933678",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ede16bcd-2819-4bc6-bbdc-20d9655e5e0b"
        },
        {
            "id": "89f8f0fe-8d5e-4b8e-947b-8b18d6911c0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ede16bcd-2819-4bc6-bbdc-20d9655e5e0b"
        },
        {
            "id": "905dc4cb-403d-4087-88f0-61e8d45a3f82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "ede16bcd-2819-4bc6-bbdc-20d9655e5e0b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}