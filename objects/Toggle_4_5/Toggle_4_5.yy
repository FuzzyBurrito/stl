{
    "id": "bce2b7f6-cdb0-4e8b-8971-2f951b71a0e9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Toggle_4_5",
    "eventList": [
        {
            "id": "94ee58d7-59d3-4fa5-ae62-27b7f669b03c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bce2b7f6-cdb0-4e8b-8971-2f951b71a0e9"
        },
        {
            "id": "45e8ce6e-ff0e-4620-9566-f75ad52d1e6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bce2b7f6-cdb0-4e8b-8971-2f951b71a0e9"
        },
        {
            "id": "6c1de462-3c1d-4f63-a560-baa879c568cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "bce2b7f6-cdb0-4e8b-8971-2f951b71a0e9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}