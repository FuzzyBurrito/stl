{
    "id": "09738fc2-b315-4cca-929b-b33acd93eaf4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Toggle_2_0",
    "eventList": [
        {
            "id": "0ff7ebed-68ff-49dd-83dc-7a2e6b2a65c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "09738fc2-b315-4cca-929b-b33acd93eaf4"
        },
        {
            "id": "5c16c067-4822-4811-ad24-5fe12b29c6e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "09738fc2-b315-4cca-929b-b33acd93eaf4"
        },
        {
            "id": "fdf58020-20e1-468e-a2b7-55ed44b42955",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "09738fc2-b315-4cca-929b-b33acd93eaf4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}