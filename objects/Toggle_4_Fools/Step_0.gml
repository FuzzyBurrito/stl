/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 0294090F
/// @DnDArgument : "var" "global.selected_version"
/// @DnDArgument : "value" ""69-pc""
if(global.selected_version == "69-pc")
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 555D8CA8
	/// @DnDParent : 0294090F
	/// @DnDArgument : "spriteind" "CheckSP"
	/// @DnDSaveInfo : "spriteind" "4fed209b-f81b-47bb-8ca4-febfc20e5900"
	sprite_index = CheckSP;
	image_index = 0;
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 7EE28E9C
else
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 7D839C97
	/// @DnDParent : 7EE28E9C
	/// @DnDArgument : "spriteind" "boxSP"
	/// @DnDSaveInfo : "spriteind" "73324b21-7bd6-49e4-ab83-159c8a59f0af"
	sprite_index = boxSP;
	image_index = 0;
}

/// @DnDAction : YoYo Games.Instances.Color_Sprite
/// @DnDVersion : 1
/// @DnDHash : 2E7E5629
/// @DnDArgument : "colour" "$FFAD07B2"
image_blend = $FFAD07B2 & $ffffff;
image_alpha = ($FFAD07B2 >> 24) / $ff;