{
    "id": "dcacdcfd-13fb-44c0-a4f6-047e4e972d4b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Toggle_4_Fools",
    "eventList": [
        {
            "id": "3ff3e033-fb5e-45e8-b6bd-e6e40e1d98bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dcacdcfd-13fb-44c0-a4f6-047e4e972d4b"
        },
        {
            "id": "85e14054-22ff-43a5-a50e-0ab3b8376774",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dcacdcfd-13fb-44c0-a4f6-047e4e972d4b"
        },
        {
            "id": "ffda0bfa-846d-4819-a604-c8bcc085865a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "dcacdcfd-13fb-44c0-a4f6-047e4e972d4b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}