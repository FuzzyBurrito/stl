{
    "id": "f618b95f-2348-4aa6-bafd-da09b2d7108e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "VersionInfoOB",
    "eventList": [
        {
            "id": "06c73e0e-f09c-4d41-b37b-9abe56112aab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f618b95f-2348-4aa6-bafd-da09b2d7108e"
        },
        {
            "id": "710906da-058e-4c81-ae5e-230183a135ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f618b95f-2348-4aa6-bafd-da09b2d7108e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aa08edd9-9183-4f46-b8ac-6ba112719e8c",
    "visible": true
}