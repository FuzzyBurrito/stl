{
    "id": "87e35c87-12c9-49c2-af19-3c3d88b42235",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "VersionsOB",
    "eventList": [
        {
            "id": "fb865e41-262e-4ac5-a75c-c4c76b5b04c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "87e35c87-12c9-49c2-af19-3c3d88b42235"
        },
        {
            "id": "7f8bc999-8a9e-48b1-abcf-aabd1e748b09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "87e35c87-12c9-49c2-af19-3c3d88b42235"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d9f40d26-910a-4eba-aa03-abc267fc2faa",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9fc4850e-1718-4325-86ed-810f61dc6f9b",
    "visible": true
}