/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 7EA3DD06
/// @DnDArgument : "var" "global.warning"
/// @DnDArgument : "value" "1"
if(global.warning == 1)
{
	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 1450DA24
	/// @DnDParent : 7EA3DD06
	/// @DnDArgument : "code" "room_goto(Versions_RM)"
	room_goto(Versions_RM)
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 11D18573
else
{
	/// @DnDAction : YoYo Games.Rooms.Go_To_Room
	/// @DnDVersion : 1
	/// @DnDHash : 4AD9143F
	/// @DnDParent : 11D18573
	/// @DnDArgument : "room" "Warning_RM"
	/// @DnDSaveInfo : "room" "bf68333f-f78c-41af-817d-e0f43375636f"
	room_goto(Warning_RM);
}