/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 079FDF6D
/// @DnDArgument : "code" "draw_self()$(13_10)$(13_10)draw_text_transformed(x, y, string("Versions") + "", 1, 1, 0);$(13_10)$(13_10)draw_text_transformed(10, 30, "STL-"+string(global.launcher_version), .6, .6, 0);"
draw_self()

draw_text_transformed(x, y, string("Versions") + "", 1, 1, 0);

draw_text_transformed(10, 30, "STL-"+string(global.launcher_version), .6, .6, 0);

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 5C575BB0
/// @DnDArgument : "var" "global.launcher_version"
/// @DnDArgument : "not" "1"
/// @DnDArgument : "value" "global.lat_launcher"
if(!(global.launcher_version == global.lat_launcher))
{
	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 542E9262
	/// @DnDParent : 5C575BB0
	/// @DnDArgument : "code" "draw_text_transformed(10,80, "Latest (STL-"+string(global.lat_launcher)+")", .6, .6, 0);"
	draw_text_transformed(10,80, "Latest (STL-"+string(global.lat_launcher)+")", .6, .6, 0);
}