/// @DnDAction : YoYo Games.Instances.Color_Sprite
/// @DnDVersion : 1
/// @DnDHash : 34A3F855
/// @DnDArgument : "colour" "$FF7B057F"
image_blend = $FF7B057F & $ffffff;
image_alpha = ($FF7B057F >> 24) / $ff;

/// @DnDAction : YoYo Games.Instances.Sprite_Image_Alpha
/// @DnDVersion : 1
/// @DnDHash : 453E888B
/// @DnDArgument : "alpha" "0"
image_alpha = 0;