{
    "id": "d9f40d26-910a-4eba-aa03-abc267fc2faa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Example1",
    "eventList": [
        {
            "id": "8596f690-b273-4da0-97f1-ae484749ad15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d9f40d26-910a-4eba-aa03-abc267fc2faa"
        },
        {
            "id": "909224f4-51cd-4fa4-b473-397b4e7d0654",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "d9f40d26-910a-4eba-aa03-abc267fc2faa"
        },
        {
            "id": "5a10a384-1c5d-4fd0-970a-10c1200e65dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "d9f40d26-910a-4eba-aa03-abc267fc2faa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9fc4850e-1718-4325-86ed-810f61dc6f9b",
    "visible": true
}