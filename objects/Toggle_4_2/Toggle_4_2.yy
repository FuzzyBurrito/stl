{
    "id": "9fa99571-3e9b-4d27-9b04-88a479213326",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Toggle_4_2",
    "eventList": [
        {
            "id": "2bb71990-322d-490d-b69c-0b519d7cb22d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9fa99571-3e9b-4d27-9b04-88a479213326"
        },
        {
            "id": "94ad8398-1703-4f9a-bac9-3a127bc91fdd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9fa99571-3e9b-4d27-9b04-88a479213326"
        },
        {
            "id": "f0a94932-c119-46ae-be93-0f9b508df76b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "9fa99571-3e9b-4d27-9b04-88a479213326"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}