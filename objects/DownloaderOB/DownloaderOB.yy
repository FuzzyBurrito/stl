{
    "id": "c97f2d01-61e4-4610-a395-51e16b0921f6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "DownloaderOB",
    "eventList": [
        {
            "id": "81219a6c-a5aa-4b25-a996-e90d3c50be47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c97f2d01-61e4-4610-a395-51e16b0921f6"
        },
        {
            "id": "16279ee8-2f2c-4775-aee9-23bfb01f8b33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c97f2d01-61e4-4610-a395-51e16b0921f6"
        },
        {
            "id": "38d33a20-a95a-4123-b87e-8206beccb64b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c97f2d01-61e4-4610-a395-51e16b0921f6"
        },
        {
            "id": "ab3254cc-1972-4d4f-aced-28c1b9d32f8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 62,
            "eventtype": 7,
            "m_owner": "c97f2d01-61e4-4610-a395-51e16b0921f6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}