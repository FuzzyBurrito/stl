{
    "id": "7bfe714d-be9e-45c6-bfdd-84aab69795ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "MainOB",
    "eventList": [
        {
            "id": "f453856e-e0d5-4d78-987d-393359c798cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7bfe714d-be9e-45c6-bfdd-84aab69795ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3709830a-2921-4a23-8d04-7a5863a86ecc",
    "visible": true
}