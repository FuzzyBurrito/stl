/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 68D52FCA
/// @DnDArgument : "code" "///Extracting your file  $(13_10)if(file_exists("N++_Installer.zip") and downloaded == false)  $(13_10){  $(13_10)  show_debug_message("Files downloaded!");  $(13_10)  zip_unzip("N++_Installer.zip", working_directory+"/Notepad++/");  $(13_10)  file_delete("N++_Installer.zip");  $(13_10)  downloaded = true;  $(13_10)}  $(13_10)else  $(13_10){  $(13_10)  alarm[2] = 15;  $(13_10)}  "
///Extracting your file  
if(file_exists("N++_Installer.zip") and downloaded == false)  
{  
  show_debug_message("Files downloaded!");  
  zip_unzip("N++_Installer.zip", working_directory+"/Notepad++/");  
  file_delete("N++_Installer.zip");  
  downloaded = true;  
}  
else  
{  
  alarm[2] = 15;  
}