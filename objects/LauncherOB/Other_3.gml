/// @DnDAction : YoYo Games.Files.Open_Ini
/// @DnDVersion : 1
/// @DnDHash : 44B68F92
/// @DnDArgument : "filename" ""config.ini""
ini_open("config.ini");

/// @DnDAction : YoYo Games.Files.Ini_Write
/// @DnDVersion : 1
/// @DnDHash : 48DBBEFE
/// @DnDArgument : "section" ""version""
/// @DnDArgument : "key" ""latest""
/// @DnDArgument : "value" "global.number"
ini_write_string("version", "latest", global.number);

/// @DnDAction : YoYo Games.Files.Close_Ini
/// @DnDVersion : 1
/// @DnDHash : 67713FE9
ini_close();