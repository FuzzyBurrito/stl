{
    "id": "67ba7fda-c303-4a55-b68a-591e189532a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "LauncherOB",
    "eventList": [
        {
            "id": "d249198f-942e-4a64-890f-635351d59525",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "67ba7fda-c303-4a55-b68a-591e189532a1"
        },
        {
            "id": "77e64505-c70a-4151-8072-31bea4d03683",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "67ba7fda-c303-4a55-b68a-591e189532a1"
        },
        {
            "id": "69453339-de56-4448-ba03-97b2e8d327cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "67ba7fda-c303-4a55-b68a-591e189532a1"
        },
        {
            "id": "018573fc-019b-473a-a918-cd4bec428d6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "67ba7fda-c303-4a55-b68a-591e189532a1"
        },
        {
            "id": "e1dc2f91-709d-47f9-867a-e8d4570e7c4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "67ba7fda-c303-4a55-b68a-591e189532a1"
        },
        {
            "id": "8d2bc2f9-018b-4bf9-838d-317c7424c360",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "67ba7fda-c303-4a55-b68a-591e189532a1"
        },
        {
            "id": "ab64f916-8bf9-4e19-b2bb-2b2e44b714d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 79,
            "eventtype": 10,
            "m_owner": "67ba7fda-c303-4a55-b68a-591e189532a1"
        },
        {
            "id": "9e1da741-786f-4ca0-b525-2fc6f928209b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "67ba7fda-c303-4a55-b68a-591e189532a1"
        },
        {
            "id": "d550e543-260d-4ea6-a97e-3131c631f676",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 10,
            "m_owner": "67ba7fda-c303-4a55-b68a-591e189532a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}