{
    "id": "2c8ad0e5-c127-4fe7-ace0-3f51631989e5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Toggle_4_1",
    "eventList": [
        {
            "id": "a1665580-79c3-445a-8950-4567d5e3c6a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2c8ad0e5-c127-4fe7-ace0-3f51631989e5"
        },
        {
            "id": "862ff81f-5607-4f90-bfb8-25c86d85821d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2c8ad0e5-c127-4fe7-ace0-3f51631989e5"
        },
        {
            "id": "aa02c025-68af-420c-a4c4-805248d6b880",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "2c8ad0e5-c127-4fe7-ace0-3f51631989e5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}