{
    "id": "a068cefd-a245-4b5b-b6ce-6093e5d36b54",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ExtractorOB",
    "eventList": [
        {
            "id": "913e7d85-c15f-4e3a-a01a-ae76f74c496f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a068cefd-a245-4b5b-b6ce-6093e5d36b54"
        },
        {
            "id": "be0ae422-0843-4262-bac5-8d668db40adc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a068cefd-a245-4b5b-b6ce-6093e5d36b54"
        },
        {
            "id": "b7a36e4d-07d5-41fb-b269-46a7649f2f27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a068cefd-a245-4b5b-b6ce-6093e5d36b54"
        },
        {
            "id": "3bd94de3-550f-4434-a11a-b180d183a326",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a068cefd-a245-4b5b-b6ce-6093e5d36b54"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}