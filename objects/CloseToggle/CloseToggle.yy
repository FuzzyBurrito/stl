{
    "id": "740c03a4-17f3-4ea0-bc7c-47696634829e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "CloseToggle",
    "eventList": [
        {
            "id": "4ff2f185-0e84-4ac6-85d5-b3cdc1ce7ba4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "740c03a4-17f3-4ea0-bc7c-47696634829e"
        },
        {
            "id": "36afa966-f2c6-4b6f-ba57-c43d6ecd6594",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "740c03a4-17f3-4ea0-bc7c-47696634829e"
        },
        {
            "id": "99200798-7739-4351-9174-cdf0e67daf31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "740c03a4-17f3-4ea0-bc7c-47696634829e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}