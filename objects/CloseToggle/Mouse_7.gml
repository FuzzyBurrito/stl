/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 1AEBB8DF
/// @DnDArgument : "var" "global.start_close"
/// @DnDArgument : "value" "1"
if(global.start_close == 1)
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 04226C50
	/// @DnDApplyTo : 6d9dd413-df41-40b3-9a3b-7eef9217ef1a
	/// @DnDParent : 1AEBB8DF
	with(rousrDissonance) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 2DEA86FD
	/// @DnDApplyTo : 40e9b2b6-6ee0-4171-80a3-1511c7ef7424
	/// @DnDParent : 1AEBB8DF
	with(MainController) instance_destroy();

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 1B917DC4
	/// @DnDParent : 1AEBB8DF
	/// @DnDArgument : "var" "global.start_close"
	global.start_close = 0;

	/// @DnDAction : YoYo Games.Common.Execute_Script
	/// @DnDVersion : 1.1
	/// @DnDHash : 4621483F
	/// @DnDParent : 1AEBB8DF
	/// @DnDArgument : "script" "Save_Config"
	/// @DnDSaveInfo : "script" "d924ac0e-9a4b-4e5d-800e-14e596bd8544"
	script_execute(Save_Config);

	/// @DnDAction : YoYo Games.Rooms.Restart_Room
	/// @DnDVersion : 1
	/// @DnDHash : 0303E45F
	/// @DnDParent : 1AEBB8DF
	room_restart();

	/// @DnDAction : YoYo Games.Common.Exit_Event
	/// @DnDVersion : 1
	/// @DnDHash : 0C10B4F7
	/// @DnDParent : 1AEBB8DF
	exit;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 62A4409D
/// @DnDArgument : "var" "global.start_close"
if(global.start_close == 0)
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 2B648C67
	/// @DnDApplyTo : 40e9b2b6-6ee0-4171-80a3-1511c7ef7424
	/// @DnDParent : 62A4409D
	with(MainController) instance_destroy();

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 0B465E01
	/// @DnDParent : 62A4409D
	/// @DnDArgument : "expr" "1"
	/// @DnDArgument : "var" "global.start_close"
	global.start_close = 1;

	/// @DnDAction : YoYo Games.Common.Execute_Script
	/// @DnDVersion : 1.1
	/// @DnDHash : 38A59559
	/// @DnDParent : 62A4409D
	/// @DnDArgument : "script" "Save_Config"
	/// @DnDSaveInfo : "script" "d924ac0e-9a4b-4e5d-800e-14e596bd8544"
	script_execute(Save_Config);

	/// @DnDAction : YoYo Games.Rooms.Restart_Room
	/// @DnDVersion : 1
	/// @DnDHash : 390E1C55
	/// @DnDParent : 62A4409D
	room_restart();

	/// @DnDAction : YoYo Games.Common.Exit_Event
	/// @DnDVersion : 1
	/// @DnDHash : 6B7F76E6
	/// @DnDParent : 62A4409D
	exit;
}