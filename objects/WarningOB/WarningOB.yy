{
    "id": "51f11a7f-e185-454d-ada0-9e641339fabe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "WarningOB",
    "eventList": [
        {
            "id": "2fed2c78-c135-43f0-bae1-7010d92cfc37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "51f11a7f-e185-454d-ada0-9e641339fabe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "84cd8f23-f8e9-49d0-9710-5db4341a3363",
    "visible": true
}