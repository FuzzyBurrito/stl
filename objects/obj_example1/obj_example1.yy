{
    "id": "7bf4a521-db8d-46ad-bda6-d2a6095ce003",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_example1",
    "eventList": [
        {
            "id": "41d334dd-8a9a-4440-9b06-e75ab8988bc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7bf4a521-db8d-46ad-bda6-d2a6095ce003"
        },
        {
            "id": "0dff24d0-db38-460e-b427-29288582cafc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7bf4a521-db8d-46ad-bda6-d2a6095ce003"
        },
        {
            "id": "743a6dda-aa91-4f1b-8736-f6d0558d908e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "7bf4a521-db8d-46ad-bda6-d2a6095ce003"
        },
        {
            "id": "35442e22-d29b-468d-8990-33a45018a235",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "7bf4a521-db8d-46ad-bda6-d2a6095ce003"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}