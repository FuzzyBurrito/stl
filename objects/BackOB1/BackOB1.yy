{
    "id": "0dfd37cc-bd57-41da-b350-30c8d402a7b6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "BackOB1",
    "eventList": [
        {
            "id": "111cf2de-205c-42b4-90df-0185dcf0c873",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0dfd37cc-bd57-41da-b350-30c8d402a7b6"
        },
        {
            "id": "7aa84d80-682a-4c93-a50f-9d05f7af1b53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0dfd37cc-bd57-41da-b350-30c8d402a7b6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d9f40d26-910a-4eba-aa03-abc267fc2faa",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9fc4850e-1718-4325-86ed-810f61dc6f9b",
    "visible": true
}