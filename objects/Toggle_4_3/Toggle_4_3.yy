{
    "id": "a750d6a8-9017-4db0-8c4e-746b43ee8304",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Toggle_4_3",
    "eventList": [
        {
            "id": "8682ec6c-0ac7-44d0-bc2b-472184b492d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a750d6a8-9017-4db0-8c4e-746b43ee8304"
        },
        {
            "id": "758b12a7-f3ef-4308-ab11-0999fb883b20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a750d6a8-9017-4db0-8c4e-746b43ee8304"
        },
        {
            "id": "a569dfd4-d0ac-454b-b257-908d19bdd3f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "a750d6a8-9017-4db0-8c4e-746b43ee8304"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}