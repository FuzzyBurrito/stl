{
    "id": "dcbec1b0-68df-4ab8-bc34-6100e1d1b415",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Toggle_4_0",
    "eventList": [
        {
            "id": "1d4b04ba-49df-4ea0-9cbd-ac118a8467a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dcbec1b0-68df-4ab8-bc34-6100e1d1b415"
        },
        {
            "id": "65f57220-1219-47fe-a6d1-9f253cf1894b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dcbec1b0-68df-4ab8-bc34-6100e1d1b415"
        },
        {
            "id": "9ba60933-dd81-4030-b0de-1b8622cd548f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "dcbec1b0-68df-4ab8-bc34-6100e1d1b415"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}