/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 1E558A6E
/// @DnDArgument : "x" "LoadOB.x-300"
/// @DnDArgument : "y" "LoadOB.y-130"
/// @DnDArgument : "caption" ""Downloading Launcher update...""
draw_text(LoadOB.x-300, LoadOB.y-130, string("Downloading Launcher update...") + "");

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 520DBFAE
/// @DnDArgument : "code" "if(!(download_done == download_size))$(13_10){$(13_10)	var health_stuff = (download_done / download_size) * 100;$(13_10)	draw_healthbar(300,370,1080,410,health_stuff,c_gray,c_purple,c_purple,180,true,true)$(13_10)}"
if(!(download_done == download_size))
{
	var health_stuff = (download_done / download_size) * 100;
	draw_healthbar(300,370,1080,410,health_stuff,c_gray,c_purple,c_purple,180,true,true)
}