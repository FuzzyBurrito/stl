{
    "id": "31217a15-0a8e-49d6-a1b1-d68b46ae5073",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "UpdateLauncher_Control",
    "eventList": [
        {
            "id": "2483c5a6-ea79-41a9-bb12-4d8b3d0df33f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "31217a15-0a8e-49d6-a1b1-d68b46ae5073"
        },
        {
            "id": "228cdcd2-5093-4e02-88b0-a39ee9208438",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "31217a15-0a8e-49d6-a1b1-d68b46ae5073"
        },
        {
            "id": "8bb6152e-3aee-4748-89fd-1bc5ab6a71e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "31217a15-0a8e-49d6-a1b1-d68b46ae5073"
        },
        {
            "id": "bda8f7bf-3c21-4c54-a8cd-f6da329c3abe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 62,
            "eventtype": 7,
            "m_owner": "31217a15-0a8e-49d6-a1b1-d68b46ae5073"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}