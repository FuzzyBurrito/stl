{
    "id": "118dc797-b893-4f0f-8c95-30ee2de8a1af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "DiscordOB",
    "eventList": [
        {
            "id": "324cdda3-63a0-4b74-9d83-a406b1c5f1c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "118dc797-b893-4f0f-8c95-30ee2de8a1af"
        },
        {
            "id": "efb60af4-1683-4741-a368-09385fc65225",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "118dc797-b893-4f0f-8c95-30ee2de8a1af"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d9f40d26-910a-4eba-aa03-abc267fc2faa",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9fc4850e-1718-4325-86ed-810f61dc6f9b",
    "visible": true
}