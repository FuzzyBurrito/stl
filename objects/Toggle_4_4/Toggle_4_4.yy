{
    "id": "74cc70f0-0085-4090-b277-2dc70927cbb9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Toggle_4_4",
    "eventList": [
        {
            "id": "fefff2ba-f3c4-46b9-876c-d2358b64dd71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "74cc70f0-0085-4090-b277-2dc70927cbb9"
        },
        {
            "id": "878a4a4e-09e2-46d9-ae04-b03ae403f7cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "74cc70f0-0085-4090-b277-2dc70927cbb9"
        },
        {
            "id": "1b3d05ba-3e9b-4f2d-b1df-a87afc26e133",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "74cc70f0-0085-4090-b277-2dc70927cbb9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73324b21-7bd6-49e4-ab83-159c8a59f0af",
    "visible": true
}