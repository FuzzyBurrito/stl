{
    "id": "153d061b-b64e-4600-a506-36764716388e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ZipButtonOB",
    "eventList": [
        {
            "id": "87c5191c-6dd7-4a48-9875-482d794624d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "153d061b-b64e-4600-a506-36764716388e"
        },
        {
            "id": "c279ea59-8d49-49a5-8366-8c0464ffed27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "153d061b-b64e-4600-a506-36764716388e"
        },
        {
            "id": "90e70374-53ef-4fd8-b576-c362c1552fe4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "153d061b-b64e-4600-a506-36764716388e"
        },
        {
            "id": "cdec1e4c-381b-4a50-9600-9933985a4039",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "153d061b-b64e-4600-a506-36764716388e"
        },
        {
            "id": "66a026bf-b75a-4442-b2a3-fdbd9e299309",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "153d061b-b64e-4600-a506-36764716388e"
        },
        {
            "id": "e170b3c3-9c4b-4aca-ae0a-2b3b0e4ee71d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "153d061b-b64e-4600-a506-36764716388e"
        },
        {
            "id": "3d986821-0489-4cfe-aede-eb72a6a051e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 79,
            "eventtype": 9,
            "m_owner": "153d061b-b64e-4600-a506-36764716388e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7321efe8-fdb5-405e-af0b-7336d88fbbac",
    "visible": true
}